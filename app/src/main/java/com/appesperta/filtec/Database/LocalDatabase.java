package com.appesperta.filtec.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.appesperta.filtec.Application;
import com.appesperta.filtec.MainActivity;
import com.appesperta.filtec.Model.AnswerMaster;
import com.appesperta.filtec.Model.ArmbandMaster;
import com.appesperta.filtec.Model.LogMaster;
import com.appesperta.filtec.Model.ProductMaster;
import com.appesperta.filtec.Model.QuestionMaster;
import com.appesperta.filtec.Model.SetList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/***
 * Created by SJM4 on 02/08/2016.
 */
public class LocalDatabase extends SQLiteOpenHelper {
    private static final String TAG = "DB";
    private static final int DATABASE_VERSION = 1;
    private static String DB_PATH = "/data/data/com.appesperta.filtec/databases/";
    private static String DB_NAME = "HardRockNov.sqlite";
    private final Context myContext;

    private DecimalFormat precision = new DecimalFormat("0.00");

    public LocalDatabase(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        this.myContext = context;
    }

    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        Log.e("dbExist", "dbExist : " + dbExist);
        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH + DB_NAME;
            if (new File(myPath).exists()) {
                checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            }
        } catch (SQLiteException e) {
            try {
                copyDataBase();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copyDataBase() throws IOException {
        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    @Override
    public synchronized void close() {
        super.close();
    }

    public void onCreate(SQLiteDatabase dp) {}

    @Override
    public void onUpgrade(SQLiteDatabase dp, int oldVersion, int newVersion) {
    }

    /***************************** SetList ****************************/
    public void addSetList(SetList setList) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(SetList.SET_ID, setList.getSetId());
        cv.put(SetList.SET_NAME, setList.getSetName());

        String query = "select * from " + SetList.TABLE_NAME + " where " + SetList.SET_ID + " = '" + setList.getSetId() + "'";
        Cursor c = db.rawQuery(query, null);
        if (c.getCount() > 0) {
            Log.e(TAG, "addSetList: update ");
            db.update(SetList.TABLE_NAME, cv, SetList.SET_ID + "='" + setList.getSetId() + "'", null);
        } else {
            db.insert(SetList.TABLE_NAME, null, cv);
        }
        c.close();
        db.close();
    }
    public void GetSetList() {
        SQLiteDatabase db = this.getReadableDatabase();
        String table = SetList.TABLE_NAME;
        String selectQuery = "SELECT * FROM " + table;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                MainActivity.setID.add(c.getString(0));
                MainActivity.setName.add(c.getString(1));
            } while (c.moveToNext());
        }
        c.close();
        db.close();
    }
    /***************************** SetList ****************************/
    /***************************** ProductMaster ****************************/
    public void addProductMaster(ProductMaster productMaster) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(ProductMaster.PRODUCT_ID, productMaster.getProductId());
        cv.put(ProductMaster.PRODUCT_NAME, productMaster.getProductName());
        cv.put(ProductMaster.PRODUCT_DES, productMaster.getDescription());
        cv.put(ProductMaster.PRODUCT_IMAGE, productMaster.getProductImage());
        cv.put(ProductMaster.PRODUCT_LINK, productMaster.getDocumentLink());
        cv.put(ProductMaster.SET_ID, productMaster.getSetId());
        cv.put(ProductMaster.QUESTION_ID, productMaster.getQuestionId());

        String query = "select * from " + ProductMaster.TABLE_NAME + " where " + ProductMaster.PRODUCT_ID + " = '" + productMaster.getProductId() + "'";
        Cursor c = db.rawQuery(query, null);
        if (c.getCount() > 0) {
            Log.e(TAG, "addProductMaster: update ");
            db.update(ProductMaster.TABLE_NAME, cv, ProductMaster.PRODUCT_ID + "='" + productMaster.getProductId() + "'", null);
        } else {
            db.insert(ProductMaster.TABLE_NAME, null, cv);
        }
        c.close();
        db.close();
    }
    public void addArmbandMaster(ArmbandMaster armbandMaster) {
        SQLiteDatabase db = this.getWritableDatabase();

        String table = ArmbandMaster.TABLE_NAME;
        ContentValues values = new ContentValues();
        values.put(ArmbandMaster.ARM_BAND_ID, armbandMaster.getArmBandID());
        values.put(ArmbandMaster.USER_ID, armbandMaster.getUserID());
        values.put(ArmbandMaster.USER_NAME, armbandMaster.getUserName());
        values.put(ArmbandMaster.MOBILE_NO, armbandMaster.getMobileNo());
        values.put(ArmbandMaster.EMAIL_ID, armbandMaster.getEmailId());
        values.put("CompanyId", Application.CompanyId);
        values.put("EventId", Application.EventId);
        values.put(ArmbandMaster.COMPANY_NAME, armbandMaster.getCompanyName());
        values.put(ArmbandMaster.Is_Deleted, armbandMaster.isDeleted());
        values.put(ArmbandMaster.Is_Sync, armbandMaster.isSync());
        values.put(ArmbandMaster.BADGE, armbandMaster.getBadge());
        values.put(ArmbandMaster.COMMENT, armbandMaster.getComment());
        values.put(ArmbandMaster.QUE1, armbandMaster.getQue1());
        values.put(ArmbandMaster.QUE2, armbandMaster.getQue2());
        values.put(ArmbandMaster.OTHER_DATA, armbandMaster.getOtherData());
        values.put(ArmbandMaster.PHONE, armbandMaster.getPhoneNo());
        values.put(ArmbandMaster.PROMOTIONAL, armbandMaster.getOptPromotional());
        values.put(ArmbandMaster.SURVEY, armbandMaster.getOptSurvey());
        values.put(ArmbandMaster.SESSIONID, armbandMaster.getSessioid());
        values.put(ArmbandMaster.SESSIONNAME, armbandMaster.getSessionname());
        String selectQuery = "SELECT  * FROM " + table + " WHERE " + ArmbandMaster.ARM_BAND_ID + "='" + armbandMaster.getArmBandID() + "' AND EventId ='" + Application.EventId + "'";
        Log.e(TAG, "addArmbandMaster: " + selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            db.update(table, values, ArmbandMaster.ARM_BAND_ID + "='" + armbandMaster.getArmBandID() + "' AND EventId ='" + Application.EventId + "'", null);
        } else {
            db.insert(table, null, values);
        }
        cursor.close();
        db.close();
    }
    public List<ProductMaster> GetProductMaster(String setId) {
        List<ProductMaster> productMasterList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String table = ProductMaster.TABLE_NAME;
        String selectQuery = "SELECT * FROM " + table +" where "+ ProductMaster.SET_ID + " = '" +setId +"'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                ProductMaster productMaster = new ProductMaster();
                productMaster.setProductId(c.getString(1));
                productMaster.setProductName(c.getString(2));
                productMaster.setDescription(c.getString(3));
                productMaster.setProductImage(c.getString(4));
                productMaster.setDocumentLink(c.getString(5));
                productMaster.setSetId(c.getString(6));
                productMaster.setQuestionId(c.getString(7));
                productMasterList.add(productMaster);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return productMasterList;
    }
    public List<ProductMaster> GetProductMasterList() {
        List<ProductMaster> productMasterList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String table = ProductMaster.TABLE_NAME;
        String selectQuery = "SELECT * FROM " + table;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                ProductMaster productMaster = new ProductMaster();
                productMaster.setProductId(c.getString(1));
                productMaster.setProductName(c.getString(2));
                productMaster.setDescription(c.getString(3));
                productMaster.setProductImage(c.getString(4));
                productMaster.setDocumentLink(c.getString(5));
                productMaster.setSetId(c.getString(6));
                productMaster.setQuestionId(c.getString(7));
                productMasterList.add(productMaster);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return productMasterList;
    }
    public String getImageLink(String productId) {
        String productImage="";
        SQLiteDatabase db = this.getReadableDatabase();
        String table = ProductMaster.TABLE_NAME;
        String selectQuery = "SELECT ProductImage as ProductImage FROM " + table + " WHERE ProductId  = '" + productId + "'";
        Log.e(TAG, "getQuestion: " + selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            productImage = c.getString(0);
        }
        c.close();
        db.close();
        return productImage;
    }
    /***************************** ProductMaster ****************************/

    /***************************** QuestionMaster     ****************************/
    public void addQuestionMaster(QuestionMaster questionMaster) {
        SQLiteDatabase db = this.getWritableDatabase();

        String table = QuestionMaster.TABLE_NAME;
        ContentValues values = new ContentValues();
        values.put(QuestionMaster.QUESTION_ID, questionMaster.getQuestionId());
        values.put(QuestionMaster.QUESTION, questionMaster.getQuestion());
        values.put(QuestionMaster.QUESTION_NO, questionMaster.getQuestionSrNo());
        values.put(QuestionMaster.DATE, questionMaster.getModifiedDate());
        values.put(QuestionMaster.ANSWER_TYPE_ID, questionMaster.getAnswerTypeId());
        values.put(QuestionMaster.ANSWER_TYPE, questionMaster.getAnswerType());
        values.put(QuestionMaster.COMMUNITY_ID, questionMaster.getCommunityResultDisplayId());
        values.put(QuestionMaster.COMMUNITY, questionMaster.getCommunityResultDisplay());
        values.put(QuestionMaster.EVENT_ID, questionMaster.getEventId());
        values.put(QuestionMaster.COMPANY_ID, questionMaster.getCompanyId());
        values.put(QuestionMaster.IS_ACTIVE, questionMaster.getIsActive());
        values.put(QuestionMaster.IS_DELETE, questionMaster.getIsDeleted());
        values.put(QuestionMaster.Set_Id, questionMaster.getSetId());
        values.put(QuestionMaster.INTERVAL, questionMaster.getInterval());

        String selectQuery = "SELECT  * FROM " + table + " WHERE " + QuestionMaster.QUESTION_ID + "='" + questionMaster.getQuestionId() + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            db.update(table, values, QuestionMaster.QUESTION_ID + "='" + questionMaster.getQuestionId() + "'", null);
        } else {
            db.insert(table, null, values);
        }
        cursor.close();
        db.close();
    }

    public QuestionMaster getQuestion(String questionId) {
        QuestionMaster questionMaster = new QuestionMaster();
        SQLiteDatabase db = this.getWritableDatabase();
        String table = QuestionMaster.TABLE_NAME;

        //String selectQuery = "SELECT * FROM " + table + " WHERE  QuestionId='" + questionId + "' AND IsActive='true' AND IsDeleted='false' ORDER BY QuestionSrNo asc";
        String selectQuery = "SELECT * FROM " + table + " WHERE  SetId='" + Application.preferences.getSetId() + "' AND IsActive='true' AND IsDeleted='false' ORDER BY QuestionSrNo asc";
        Log.e(TAG, "getQuestion: " + selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        Log.e(TAG, "getQuestion: count "+c.getCount());
        if (c.moveToFirst()) {
            do {
                questionMaster.setQuestionId(c.getString(0));
                questionMaster.setQuestion(c.getString(1));
                questionMaster.setQuestionSrNo(c.getString(2));
                questionMaster.setModifiedDate(c.getString(3));
                questionMaster.setAnswerTypeId(c.getString(4));
                questionMaster.setAnswerType(c.getString(5));
                questionMaster.setCommunityResultDisplayId(c.getString(6));
                questionMaster.setCommunityResultDisplay(c.getString(7));
                questionMaster.setEventId(c.getString(8));
                questionMaster.setCompanyId(c.getString(9));
                questionMaster.setIsActive(c.getString(10));
                questionMaster.setIsDeleted(c.getString(11));
                questionMaster.setSetId(c.getString(12));
                questionMaster.setInterval(c.getString(15));
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return questionMaster;
    }

    public List<ArmbandMaster> getArmbandList() {
        List<ArmbandMaster> armbandList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String table = ArmbandMaster.TABLE_NAME;
        String selectQuery = "SELECT * FROM " + table + " WHERE " + ArmbandMaster.Is_Deleted + " = '0' AND " + ArmbandMaster.Is_Sync + "= '0'";
        Log.e(TAG, "getAnswerList: " + selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        Log.e(TAG, "getAnswerList: " + c.getCount());
        if (c.moveToFirst()) {
            do {
                ArmbandMaster armbandMaster = new ArmbandMaster();
                armbandMaster.setArmBandID(c.getString(1));
                armbandMaster.setUserID(c.getString(2));
                armbandMaster.setUserName(c.getString(3).trim());
                armbandMaster.setMobileNo(c.getString(4));
                armbandMaster.setEmailId(c.getString(5));
                armbandMaster.setCompanyName(c.getString(8));
                armbandMaster.setBadge(c.getString(11));
                armbandMaster.setComment(c.getString(12));
                armbandMaster.setQue1(c.getString(13));
                armbandMaster.setQue2(c.getString(14));
                armbandMaster.setOtherData(c.getString(15));
                armbandMaster.setPhoneNo(c.getString(16));
                armbandMaster.setOptPromotional(c.getString(17));//que3
                armbandMaster.setOptSurvey(c.getString(18));
                armbandMaster.setSessioid(c.getString(19));
                armbandMaster.setSessionname(c.getString(20));
                armbandList.add(armbandMaster);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return armbandList;
    }

    /***************************** QuestionMaster ****************************/


    /***************************** AnswerMaster ****************************/
    public void addAnswerMaster(AnswerMaster answerMaster) {
        SQLiteDatabase db = this.getWritableDatabase();

        String table = AnswerMaster.TABLE_NAME;
        ContentValues values = new ContentValues();
        values.put(AnswerMaster.ANSWER_ID, answerMaster.getAnswerId());
        values.put(AnswerMaster.EVENT_ID, answerMaster.getEventId());
        values.put(AnswerMaster.QUESTION_ID, answerMaster.getQuestionId());
        values.put(AnswerMaster.COMPANY_ID, answerMaster.getCompanyId());
        values.put(AnswerMaster.ANSWER, answerMaster.getAnswer());
        values.put(AnswerMaster.DATE, answerMaster.getModifiedDate());
        values.put(AnswerMaster.RESULT_TITLE, answerMaster.getResultTitle());
        values.put(AnswerMaster.Asm_Value, answerMaster.getAsmValue());
        values.put(AnswerMaster.RESULT_Desc, answerMaster.getResultDesc());
        values.put(AnswerMaster.ANS_IMAGE, answerMaster.getAnsImage());
        values.put(AnswerMaster.PIE_DESC, answerMaster.getPieAnswerDescription());
        values.put(AnswerMaster.IS_ACTIVE, answerMaster.getIsActive());
        values.put(AnswerMaster.IS_DELETE, answerMaster.getIsDeleted());
        values.put(AnswerMaster.Display_Order, answerMaster.getDisplayOrder());
        String selectQuery = "SELECT  * FROM " + table + " WHERE " + AnswerMaster.ANSWER_ID + "='" + answerMaster.getAnswerId() + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            db.update(table, values, AnswerMaster.ANSWER_ID + "='" + answerMaster.getAnswerId() + "'", null);
        } else {
            db.insert(table, null, values);
        }
        cursor.close();
        db.close();
    }

    public List<AnswerMaster> getAnswerList(String questionId) {
        List<AnswerMaster> answerMastersList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String table = AnswerMaster.TABLE_NAME;
        String selectQuery = "SELECT * FROM " + table + " WHERE QuestionId = '" + questionId + "' AND IsActive='true' AND IsDeleted='false' ORDER BY " + AnswerMaster.Display_Order + " ASC";
        Log.e(TAG, "getAnswerList: " + selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                AnswerMaster answerMaster = new AnswerMaster();
                answerMaster.setAnswerId(c.getString(0));
                answerMaster.setEventId(c.getString(1));
                answerMaster.setQuestionId(c.getString(2));
                answerMaster.setCompanyId(c.getString(3));
                answerMaster.setAnswer(c.getString(4));
                answerMaster.setModifiedDate(c.getString(5));
                answerMaster.setResultTitle(c.getString(6));
                answerMaster.setAsmValue(c.getString(7));
                answerMaster.setResultDesc(c.getString(8));
                answerMaster.setAnsImage(c.getString(9));
                answerMaster.setPieAnswerDescription(c.getString(10));
                answerMaster.setIsActive(c.getString(11));
                answerMaster.setIsDeleted(c.getString(12));
                answerMaster.setDisplayOrder(c.getString(13));
                answerMastersList.add(answerMaster);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return answerMastersList;
    }

    public String getResultDesc(String answerId) {
        String resultDesc = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String table = AnswerMaster.TABLE_NAME;
        String selectQuery = "SELECT ResultDesc as resultDesc FROM " + table + " WHERE AnswerId  = '" + answerId + "'";
        Log.e(TAG, "getQuestion: " + selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            resultDesc = c.getString(0);
        }
        c.close();
        db.close();
        return resultDesc;
    }

    public String getPieAnswerDesc(String answerId) {
        String resultDesc = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String table = AnswerMaster.TABLE_NAME;
        String selectQuery = "SELECT PieAnswerDescription as PieAnswerDesc FROM " + table + " WHERE AnswerId  = '" + answerId + "'";
        Log.e(TAG, "getQuestion: " + selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            resultDesc = c.getString(0);
        }
        c.close();
        db.close();
        return resultDesc;
    }

    public String getResultTitle(String answerId) {
        String ResultTitle = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String table = AnswerMaster.TABLE_NAME;
        String selectQuery = "SELECT ResultTitle as ResultTitle FROM " + table + " WHERE AnswerId  = '" + answerId + "'";
        Log.e(TAG, "getQuestion: " + selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            ResultTitle = c.getString(0);
        }
        c.close();
        db.close();
        return ResultTitle;
    }

    public String getAnsImage(String answerId) {
        String AnsImage = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String table = AnswerMaster.TABLE_NAME;
        String selectQuery = "SELECT AnsImage as AnsImage FROM " + table + " WHERE AnswerId  = '" + answerId + "'";
        Log.e(TAG, "getQuestion: " + selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            AnsImage = c.getString(0);
        }
        c.close();
        db.close();
        return AnsImage;
    }

    public String getAsmValue(String answerId) {
        String AsmValue = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String table = AnswerMaster.TABLE_NAME;
        String selectQuery = "SELECT AsmValue as AsmValue FROM " + table + " WHERE AnswerId  = '" + answerId + "'";
        Log.e(TAG, "getQuestion: " + selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            AsmValue = c.getString(0);
        }
        c.close();
        db.close();
        return AsmValue;
    }
    public void updateSummaryDatabase() {
        SQLiteDatabase db = this.getWritableDatabase();
        String table = LogMaster.KEY_TABLE_NAME;
        db.delete(table, LogMaster.KEY_MODIFIED_DATE + "=''", null);
        db.close();
    }

    /***************************** AnswerMaster ****************************/

    /***************************** LogMaster ****************************/
    public void addLogMaster(LogMaster logMaster, boolean isSync) {
        Log.e(TAG, "addLogMaster: " + isSync);
        Log.e(TAG, "addLogMaster: " + logMaster.toString());
        SQLiteDatabase db = this.getWritableDatabase();

        // String logId = UUID.randomUUID().toString();
        String table = LogMaster.KEY_TABLE_NAME;
        ContentValues values = new ContentValues();
        //values.put(LogMaster.KEY_LOG_ID, "");
        values.put(LogMaster.KEY_QUESTION_ID, logMaster.getQuestionId());
        values.put(LogMaster.KEY_ANSWER_ID, logMaster.getAnswerId());
        values.put(LogMaster.KEY_USER_ID, "");
        values.put(LogMaster.KEY_COMPANY_ID, logMaster.getCompanyId());
        values.put(LogMaster.KEY_EVENT_ID, logMaster.getEventId());
        values.put(LogMaster.KEY_ASM_VALUE, logMaster.getAsmValue());
        values.put(LogMaster.KEY_ANSWER, logMaster.getAnswer());
        values.put(LogMaster.KEY_ANSWER_IMAGE, logMaster.getAnswerImage());
        values.put(LogMaster.KEY_PIE_ANSWER_DESC, logMaster.getPieAnswerDesc());
        values.put(LogMaster.KEY_RESULT_DESC, logMaster.getResultDesc());
        values.put(LogMaster.KEY_RESULT_TITLE, logMaster.getResultTitle());
        values.put(LogMaster.KEY_IS_SYNC, isSync);
        values.put(LogMaster.KEY_MODIFIED_DATE, logMaster.getModifiedDate());
        values.put(LogMaster.KEY_ARMBAND_ID, logMaster.getArmBandId());
        values.put(LogMaster.KEY_SET_ID, logMaster.getSetId());
        values.put(LogMaster.KEY_PRODUCT_ID, logMaster.getProductId());

        String selectQuery = "SELECT  * FROM " + table + " WHERE " + LogMaster.KEY_ARMBAND_ID + "='" + logMaster.getArmBandId() + "' AND " + LogMaster.KEY_ANSWER_ID + " ='" + logMaster.getAnswerId() + "'";
        Log.e(TAG, "addArmbandMaster: " + selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            db.update(table, values, LogMaster.KEY_ARMBAND_ID + "='" + logMaster.getArmBandId() + "' AND " + LogMaster.KEY_ANSWER_ID + " ='" + logMaster.getAnswerId() + "'", null);
        } else {
            long a = db.insert(table, null, values);
            Log.e(TAG, "addLogMaster: " + a);
        }
        cursor.close();
        db.close();
    }
    public String getCommunityResult(String questionId, String answerId) {
        String communityResult;
        int totalCount = 0;
        int YourCount = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String table = LogMaster.KEY_TABLE_NAME;
        String selectQuery = "SELECT COUNT(*) AS count FROM " + table + " Where QuestionId =  '" + questionId + "'";
        Log.e(TAG, "getQuestion: " + selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            totalCount = c.getInt(0);
        }
        c.close();
        String selectQuery1 = "SELECT COUNT(*) AS count FROM " + table + " Where QuestionId =  '" + questionId + "' and AnswerId = '" + answerId + "'";
        Log.e(TAG, "getQuestion: " + selectQuery1);
        Cursor c1 = db.rawQuery(selectQuery1, null);
        if (c1.moveToFirst()) {
            YourCount = c1.getInt(0);
        }
        c1.close();
        db.close();
        double d = ((double) YourCount / totalCount) * 100;
        if (d % 1 == 0) {
            communityResult = d + "";
        } else {
            communityResult = precision.format(d);
        }
        Log.e(TAG, "getCommunityResult: " + communityResult);
        return communityResult;
    }

    public String getPieDesc(String answerId) {
        String image = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String table = LogMaster.KEY_TABLE_NAME;
        String selectQuery1 = "SELECT PieAnswerDesc FROM " + table + " WHERE AnswerId ='" + answerId + "'  LIMIT 1";
        Log.e(TAG, "getQuestion: " + selectQuery1);
        Cursor c1 = db.rawQuery(selectQuery1, null);
        if (c1.moveToFirst()) {
            image = c1.getString(0);
        }
        c1.close();
        db.close();
        return image;
    }

    public String getDate(String query) {
        String date = "";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        if (c.moveToFirst()) {
            date = c.getString(c.getColumnIndex("date"));
        }
        Log.e(TAG, "getDate: date " + date);
        c.close();
        db.close();
        return date;
    }
    public List<LogMaster> getSummaryList(String query) {
        List<LogMaster> summaryList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Log.e(TAG, "getSummaryList: query " + query);
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                LogMaster logMaster = new LogMaster();
                logMaster.setQuestionId(c.getString(0));
                logMaster.setAnswerId(c.getString(1));
                logMaster.setCompanyId(c.getString(2));
                logMaster.setArmBandId(c.getString(3));
                logMaster.setEventId(c.getString(4));
                logMaster.setSetId(c.getString(5));
                logMaster.setProductId(c.getString(6));
                summaryList.add(logMaster);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return summaryList;
    }


}
