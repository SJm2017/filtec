package com.appesperta.filtec.utils;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appesperta.filtec.Application;
import com.appesperta.filtec.R;

import java.util.List;

public class MySpinnerAdapter extends ArrayAdapter<String> {

    Context context;
    List<String> items;

    public MySpinnerAdapter(Context context, int resource, List<String> items) {
        super(context, resource, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTextColor(context.getResources().getColor(R.color.colorAccent));
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, (13 * Application.dip));
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTextColor(context.getResources().getColor(R.color.colorAccent));
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, (13 * Application.dip));
        if (position == (items.size() - 1)) {
            view.setBackground(Application.rectangle_bottom());
        } else {
            view.setBackground(Application.rectangle_middle());
        }
        return view;
    }
}