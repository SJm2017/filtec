package com.appesperta.filtec.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

    private SharedPreferences pref;
    // Editor for Shared preferences
    private SharedPreferences.Editor editor;

    private static final String PREF_NAME  = "Filtec";
    private static final String SetId   = "sSetId";
    private static final String NfcId   = "nfcId";
    private static final String UserName   = "userName";
    private static final String UserId  = "userId";

    public Preferences(Context contx){
        pref = contx.getSharedPreferences(PREF_NAME, 0);
        editor = pref.edit();
    }

    public void ClearPreference() {
        editor.clear();
        editor.commit();
    }

    public void setSetId(String s) {
        editor.putString(SetId, s);
        editor.commit();
    }
    public String getSetId(){
        return pref.getString(SetId, "");
    }

    public void setNfcId(String s) {
        editor.putString(NfcId, s);
        editor.commit();
    }
    public String getNfcId(){
        return pref.getString(NfcId, "");
    }

    public void setUserName(String userName) {
        editor.putString(UserName, userName);
        editor.commit();
    }
    public String getUserName() {
        return pref.getString(UserName, "");
    }

    public void setUserId(String userId) {
        editor.putString(UserId, userId);
        editor.commit();
    }
    public String getUserId() {
        return pref.getString(UserId, "");
    }

}
