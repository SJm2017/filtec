package com.appesperta.filtec.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;


import com.appesperta.filtec.Application;
import com.appesperta.filtec.Database.LocalDatabase;
import com.appesperta.filtec.Model.ArmbandMaster;
import com.appesperta.filtec.Retrofit.ApiService;
import com.appesperta.filtec.Retrofit.RetroClient;

import org.json.JSONObject;

import java.util.List;
import java.util.StringTokenizer;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/*** Created by SJM4 on 03/09/2016.*/
public class SyncService implements Runnable {
    private static final String TAG = SyncService.class.getSimpleName();
    private LocalDatabase DB;
    private Context context;


    public SyncService(Context context) {
        this.context = context;
        this.DB = new LocalDatabase(context.getApplicationContext());
    }

    @Override
    public void run() {
        DB = new LocalDatabase(context.getApplicationContext());
        List<ArmbandMaster> armbandList = DB.getArmbandList();
        Log.e(TAG, "getArmbandList : " + armbandList.size());
        Application.Sync_r = true;

        if (InternetConnection.checkConnection(context.getApplicationContext())) {
            ApiService api = RetroClient.getApiService();
            for (int i = 0; i < armbandList.size(); i++) {
                Log.e("getUserName", "getUserName : " + armbandList.get(i).getUserName());
                StringTokenizer stringTokenizer = new StringTokenizer(armbandList.get(i).getUserName(), " ");
                String fName, lName, Email = "";
                if (stringTokenizer.countTokens() >= 2) {
                    fName = stringTokenizer.nextToken();
                    lName = stringTokenizer.nextToken();
                } else {
                    fName = stringTokenizer.nextToken();
                    lName = "";
                }


                Call<String> call = api.doRegistration(Application.CompanyId, Application.EventId,
                        armbandList.get(i).getArmBandID(), fName, lName, armbandList.get(i).getMobileNo(),
                        armbandList.get(i).getEmailId(), armbandList.get(i).getCompanyName(), armbandList.get(i).getBadge(), "Flitec");

                final ArmbandMaster armbandMaster = armbandList.get(i);
                Log.e(TAG, "doRegistration: "+call.request().url().toString() );
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        Log.e(TAG, "onResponse: " + response.message());
                        Log.e(TAG, "onResponse: " + response.errorBody());
                        parseJson(response.body(), armbandMaster);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.e(TAG, "onFailure: " + t.getMessage());
                    }
                });
            }
        }
        Application.getInstant().getHandler_R().postDelayed(this, 60000);
    }

    private void parseJson(String response, ArmbandMaster armbandMaster) {
        if (!TextUtils.isEmpty(response)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                boolean code = jsonObject.optBoolean("Flag");
                Log.e(TAG, "parseJson: response " + jsonObject.toString());
                if (code) {
                    armbandMaster.setSync(true);
                    armbandMaster.setUserID(jsonObject.optString("UserId"));
                    DB.addArmbandMaster(armbandMaster);


                    if (!armbandMaster.getOtherData().isEmpty()) {
                        try {
                           // JSONObject badgeData = new JSONObject(armbandMaster.getOtherData().replace("BCARDData", ""));
                            JSONObject jsonleadinfo = new JSONObject();
                            jsonleadinfo.put("UserId", armbandMaster.getUserID());
                            jsonleadinfo.put("ArmBandId", armbandMaster.getArmBandID());
                            jsonleadinfo.put("EventId", Application.EventId);
                            jsonleadinfo.put("CompanyId", Application.CompanyId);

                            StringTokenizer stringTokenizer = new StringTokenizer(armbandMaster.getUserName(), " ");
                            String fName, lName;
                            if (stringTokenizer.countTokens() >= 2) {
                                fName = stringTokenizer.nextToken();
                                lName = stringTokenizer.nextToken();
                            } else {
                                fName = stringTokenizer.nextToken();
                                lName = "";
                            }


                            StringTokenizer st = new StringTokenizer(armbandMaster.getOtherData(), "^");
                            //String str[] = data.split("^");
                            String sBadgeID = st.nextToken();
                            String sCustomerID = st.nextToken();
                            String sFirstName = st.nextToken();
                            String sLastName = st.nextToken();
                            String sTitle = st.nextToken();
                            String sCompany = st.nextToken();
                            String sAddress1 = st.nextToken();
                            String sAddress2 = st.nextToken();
                            String sAddress3 = st.nextToken();
                            String sCity = st.nextToken();
                            String sState = st.nextToken();
                            String sZip = st.nextToken();
                            String sCountry = st.nextToken();
                            String sPhone = st.nextToken();
                            String sMobile = st.nextToken();
                            String sEmail = st.nextToken();
                            String sRegType = st.nextToken();
                            String sVoucher = st.nextToken();



                            jsonleadinfo.put("FirstName", fName);
                            jsonleadinfo.put("LastName", lName);
                            jsonleadinfo.put("Title",sTitle );
                            jsonleadinfo.put("Company", armbandMaster.getCompanyName());

                           /* jsonleadinfo.put("Question", context.getString(R.string.do_you_develop_games_for_mobile));
                            jsonleadinfo.put("Answer", armbandMaster.getQue1());
                            jsonleadinfo.put("HotLead", armbandMaster.getQue2());*/

                            jsonleadinfo.put("Address1", sAddress1);
                            jsonleadinfo.put("Address2",sAddress2);
                            jsonleadinfo.put("Address3", sAddress3);

                            jsonleadinfo.put("Email", armbandMaster.getEmailId());
                            jsonleadinfo.put("City", sCity);
                            jsonleadinfo.put("State", sState);
                            jsonleadinfo.put("Country", sCountry);
                            jsonleadinfo.put("ZipCode", sZip);
                            jsonleadinfo.put("Phone", sPhone);
                            jsonleadinfo.put("PhoneExtension", "");
                            jsonleadinfo.put("opt_promotional_events", "false");
                            jsonleadinfo.put("opt_survey", "false");

                            String str = "RegType" + "^^" + sRegType + "~~" +
                                    "Voucher" + "^^" + sVoucher  + "~~" +
                                    "CustomerID" + "^^" + sCustomerID  + "";

                            jsonleadinfo.put("OtherInformation", str);

                            JSONObject jsonOutPutObj = new JSONObject();
                            jsonOutPutObj.put("obj", jsonleadinfo);
                            Log.e(TAG, "run: jsonObject " + jsonOutPutObj.toString());
                            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonOutPutObj.toString());
                            ApiService api = RetroClient.getApiService();
                            Call<String> call = api.SaveUserInfo(body);
                            call.enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    Log.e(TAG, "onResponse: " + response.body());
                                    // DB.updateSummaryDatabase();

                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    Log.e(TAG, "onFailure: " + t.getMessage());

                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if (!armbandMaster.getComment().isEmpty()) {
                        try {
                            JSONObject j = new JSONObject();
                            JSONObject j1 = new JSONObject();
                            j1.put("UserId", armbandMaster.getUserID());
                            j1.put("CompanyId", Application.CompanyId);
                            j1.put("EventId", Application.EventId);
                            j1.put("Comment", armbandMaster.getComment());
                            j.put("objcomments", j1);

                            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), j.toString());
                            ApiService api = RetroClient.getApiService();
                            Call<String> call = api.SaveComments(body);
                            call.enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    Log.e(TAG, "onResponse: " + response.body());
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    Log.e(TAG, "onFailure: " + t.getMessage());
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "parseJson: response Null");
        }
    }
}
