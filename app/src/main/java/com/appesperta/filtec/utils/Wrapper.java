package com.appesperta.filtec.utils;

import com.appesperta.filtec.Model.AnswerMaster;

import java.io.Serializable;
import java.util.List;

/**** Created by Android-1 on 12/3/2016.*/

public class Wrapper implements Serializable {

    private transient List<AnswerMaster> answerMasterList;

    public Wrapper(List<AnswerMaster> answerMasterList) {
        this.answerMasterList = answerMasterList;
    }

    public List<AnswerMaster> getAnswerMasterList() {
        return answerMasterList;
    }
}
