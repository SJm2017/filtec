package com.appesperta.filtec;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.appesperta.filtec.Database.LocalDatabase;
import com.appesperta.filtec.Main.MainPresenterImpl;
import com.appesperta.filtec.Main.MainView;
import com.appesperta.filtec.utils.MySpinnerAdapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainView {

    private static final String TAG = "MainActivity";
    @BindView(R.id.spinner_set)
    Spinner spinner_set;
    @BindView(R.id.frm_spin)
    FrameLayout frm_spin;
    @BindView(R.id.btn_go)
    Button btn_go;
    @BindView(R.id.btn_registration)
    Button btn_registration;

    ProgressDialog pd;
    WeakReference<Activity> loginActivityWeakRef;

    public static ArrayList<String> setID = new ArrayList<>();
    public static ArrayList<String> setName = new ArrayList<>();

    private MainPresenterImpl mainPresenterImpl;
    LocalDatabase DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mainPresenterImpl = new MainPresenterImpl(this, this);
        DB = new LocalDatabase(getApplicationContext());
        try {
            DB.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCamera = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA);
            int writePermission = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int readPermission = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);

            if (permissionCamera != PackageManager.PERMISSION_GRANTED && writePermission != PackageManager.PERMISSION_GRANTED && readPermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
            } else {
                init();
            }
        } else {
            init();
        }

        frm_spin.setBackground(Application.rectangle(MainActivity.this));

        btn_go.setText(getResources().getString(R.string.login));
        btn_go.setTextColor(Color.parseColor(Application.textColor));
        btn_go.setBackground(Application.rectangle_btn(Application.themeColor));
        btn_go.setPadding(225, 7, 225, 7);
        btn_go.setTextSize(TypedValue.COMPLEX_UNIT_PX, (20 * Application.dip));

        btn_registration.setText(getResources().getString(R.string.registration));
        btn_registration.setTextColor(Color.parseColor(Application.textColor));
        btn_registration.setBackground(Application.rectangle_btn(Application.themeColor));
        btn_registration.setPadding(35, 7, 35, 7);
        btn_registration.setTextSize(TypedValue.COMPLEX_UNIT_PX, (20 * Application.dip));

        try {
            File sd = Environment.getExternalStorageDirectory();
            if (sd.canWrite()) {
                String currentDBPath = "/data/data/" + getPackageName() + "/databases/HardRockNov.sqlite";
                String backupDBPath = "Filtec.sqlite";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);
                Log.e(TAG, "currentDB 111 " + backupDB);
                if (currentDB.exists()) {
                    Log.e(TAG, "currentDB " + backupDB);
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void init() {
        mainPresenterImpl.getSet();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Please Continue", "Please Continue");
                    init();
                } else {
                    finish();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @OnClick(R.id.btn_go)
    public void onCLickGo() {

        navigateToHome();

    }
    @OnClick(R.id.btn_registration)
    public void onCLickRegistration() {
        navigateToRegistration();
    }

    @Override
    public void showProgressDialog() {
        loginActivityWeakRef = new WeakReference<Activity>(MainActivity.this);
        if (loginActivityWeakRef != null && !loginActivityWeakRef.get().isFinishing()) {
            pd = new ProgressDialog(MainActivity.this);
            pd.setMessage(getString(R.string.please_wait));
            pd.setIndeterminate(false);
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.show();
        }
    }

    @Override
    public void hideProgressDialog() {
        try {
            if (pd != null && pd.isShowing()) {
                pd.dismiss();
            }
        } catch (final IllegalArgumentException e) {
            // Handle or log or ignore
            e.printStackTrace();
        } catch (final Exception e) {
            // Handle or log or ignore
        } finally {
            pd = null;
        }
    }

    @Override
    public void showSnackBar(String resId) {
        Toast.makeText(getApplicationContext(), resId, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToHome() {
        if (!Application.Sync_s) {
            Application.getInstant().getThread().start();
        }
        Application.getInstant().getThread_s().start();
        Intent i = new Intent(getApplicationContext(), ProductActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void navigateToRegistration() {
        Intent intent = new Intent(MainActivity.this, Barcode_RegActivity.class);
        startActivity(intent);
    }

    @Override
    public void selectSets() {
        MySpinnerAdapter ageAdapter = new MySpinnerAdapter(this, android.R.layout.simple_spinner_item, setName);
        ageAdapter.setDropDownViewResource(android.R.layout.simple_selectable_list_item);
        spinner_set.setAdapter(ageAdapter);
        spinner_set.setOnItemSelectedListener(spinner_select);
    }

    AdapterView.OnItemSelectedListener spinner_select = new AdapterView.OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            Log.e(TAG, "onItemClick: " + spinner_set.getSelectedItemPosition());
            Application.preferences.setSetId(setID.get(spinner_set.getSelectedItemPosition()));
        }

        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
}
