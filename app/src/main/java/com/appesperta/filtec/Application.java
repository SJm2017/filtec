package com.appesperta.filtec;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;

import com.appesperta.filtec.Service.SaveSyncThread;
import com.appesperta.filtec.Service.SyncService;
import com.appesperta.filtec.Service.SyncThread;
import com.appesperta.filtec.utils.*;

public class Application extends android.app.Application {
    public static Preferences preferences;
    public static float screenInches,dip;
    public static String CompanyId = "39";
    public static String EventId = "25";
    public static String themeColor = "#003A60";
    public static String textColor = "#ffffff";
    public static int cameraId = 1;
    public static boolean Sync_r = false;
    public static boolean Sync_s = false;
    private static Application mInstant;
    public static boolean isUpdate;
    private Handler handler_R = new Handler();
    private Handler handler = new Handler();
    Thread thread_r;
    Thread thread_s;
    Thread thread;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstant = this;
        preferences = new Preferences(getApplicationContext());
        screenInches = getResources().getDisplayMetrics().widthPixels;
        Log.e("debug", "Screen inches : " + screenInches);
        Log.e("debug", "Screen inches : " + getResources().getDisplayMetrics().heightPixels);
        dip = screenInches/480;
        Log.e("debug", "Screen dip : " + dip);
    }

   public static int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    public static Drawable CircleBorder(int width, int height) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.parseColor(themeColor));
        drawable.setShape(GradientDrawable.OVAL);
        drawable.setStroke((int) dpToPx(3), Color.parseColor(textColor));
        drawable.setSize((int) dpToPx(width), (int) dpToPx(height));
        return drawable;
    }

    public static Drawable Circle(int width, int height) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.parseColor(themeColor));
        drawable.setShape(GradientDrawable.OVAL);
        drawable.setSize((int) dpToPx(width), (int) dpToPx(height));
        return drawable;
    }

    public static StateListDrawable makeSelector_new(int width, int height) {
        StateListDrawable res = new StateListDrawable();
        res.addState(new int[]{android.R.attr.state_checked}, Circle_text((int)dpToPx(width),(int)dpToPx(height)));
        res.addState(new int[]{android.R.attr.state_checkable}, CircleBorder((int)dpToPx(width),(int)dpToPx(height)));
        res.addState(new int[]{}, CircleBorder((int)dpToPx(width),(int)dpToPx(height)));

        return res;
    }

    public static Drawable Circle_text(int width, int height) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.parseColor(textColor));
        drawable.setShape(GradientDrawable.OVAL);
        drawable.setSize((int) dpToPx(width), (int) dpToPx(height));
        return drawable;
    }

    public static StateListDrawable makeSelector(int width, int height) {
        StateListDrawable res = new StateListDrawable();
        res.setExitFadeDuration(400);
        res.addState(new int[]{android.R.attr.state_pressed}, Circle((int) dpToPx(width),(int)dpToPx(height)));
        res.addState(new int[]{}, CircleBorder((int)dpToPx(width),(int)dpToPx(height)));
        return res;
    }

    public static ColorStateList selectorText() {
        return new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_pressed },
                        new int[]{}
                },
                new int[]{
                        Color.parseColor(textColor),
                        Color.parseColor(themeColor)});
    }

   public static float dpToPx(float dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return pxToDp(Math.round(px));
    }

    private static float pxToDp(float px) {
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return Math.round(dp);
    }
    public static Drawable dashLine(Drawable drawable) {
        LayerDrawable shape = (LayerDrawable) drawable;
        shape.setColorFilter(Color.parseColor(textColor), PorterDuff.Mode.SRC_ATOP);
        return shape;
    }

    public static Drawable rectangle(Context context){
        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(context.getResources().getColor(R.color.colorBackground));
        gd.setStroke((int) dpToPx(2), Color.parseColor(themeColor));
        gd.setCornerRadius((int) dpToPx(10));
        return gd;
    }

    public static Drawable rectangle_bottom(){
        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.TRANSPARENT);
        gd.setStroke((int) dpToPx(2), Color.parseColor(themeColor));
        Drawable[] layers = {gd};
        LayerDrawable layerDrawable = new LayerDrawable(layers);
        layerDrawable.setLayerInset(0, 1, -2, 1, 1);
        return layerDrawable;
    }

    public static Drawable rectangle_middle(){
        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.TRANSPARENT);
        gd.setStroke((int) dpToPx(2), Color.parseColor(themeColor));
        Drawable[] layers = {gd};
        LayerDrawable layerDrawable = new LayerDrawable(layers);
        layerDrawable.setLayerInset(0, 1, -2, 1, -2);
        return layerDrawable;
    }
    public static Drawable rectangle_btn(String color){
        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor(color));
        gd.setStroke((int) dpToPx(2), Color.parseColor(color));
        return gd;
    }
    public static Application getInstant(){
        return mInstant;
    }

    public Handler getHandler_R(){
        return handler_R;
    }
    public Thread getThread_r(){
        return thread_r = new Thread(new SyncService(getApplicationContext()));
    }

    public Thread getThread_s(){
        return thread_s = new Thread(new SyncThread(getApplicationContext()));
    }

    public Handler getHandler(){
        return handler;
    }
    public Thread getThread(){
        return  thread = new Thread(new SaveSyncThread(getApplicationContext()));
    }
}
