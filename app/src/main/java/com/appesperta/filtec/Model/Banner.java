package com.appesperta.filtec.Model;

/*** Created by Jitesh Dalsaniya on 15-Dec-16.*/

public class Banner {

    public static final String KEY_TABLE_NAME = "banner";

    public static final String KEY_BANNER_NAME = "BannerName";
    public static final String KEY_BANNER_TYPE = "BannerType";
    public static final String KEY_BANNER_IMAGE = "BannerImage";
    public static final String KEY_BANNER_SLIDE_TIME = "RotationTime";
    public static final String KEY_MODIFIED_DATE = "ModifiedDate";
    public static final String KEY_DISPLAY_ORDER = "DisplayOrder";

    private String BannerName, BannerType,BannerImage, RotationTime,ModifiedDate;
    private int DisplayOrder;

    public String getBannerName() {
        return BannerName;
    }
    public void setBannerName(String bannerName) {
        BannerName = bannerName;
    }

    public String getBannerType() {
        return BannerType;
    }
    public void setBannerType(String bannerType) {
        BannerType = bannerType;
    }

    public String getBannerImage() {
        return BannerImage;
    }
    public void setBannerImage(String bannerImage) {
        BannerImage = bannerImage;
    }

    public String getRotationTime() {
        return RotationTime;
    }
    public void setRotationTime(String rotationTime) {
        RotationTime = rotationTime;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }
    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public int getDisplayOrder() {
        return DisplayOrder;
    }
    public void setDisplayOrder(int displayOrder) {
        DisplayOrder = displayOrder;
    }

}
