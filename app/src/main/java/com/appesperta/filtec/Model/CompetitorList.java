package com.appesperta.filtec.Model;

import org.json.JSONObject;

/*Created by Android-1 on 2/3/2017.*/

public class CompetitorList {

    public static final String TABLE_NAME = "CompetitorList";
    public static final String COMPETITOR_NAME = "CompititorName";

    private String CompetitorName;

    public CompetitorList(JSONObject j) {
        try {
            setCompetitorName(j.optString(COMPETITOR_NAME));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getCompetitorName() {
        return CompetitorName;
    }
    public void setCompetitorName(String competitorName) {
        CompetitorName = competitorName;
    }

}
