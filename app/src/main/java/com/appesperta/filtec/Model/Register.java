package com.appesperta.filtec.Model;

public class Register {

    public static final String TABLE_NAME = "TblArmbandRegistration";

    public static final String ARM_BAND_ID = "ArmBandID";
    public static final String USER_ID = "UserID";
    public static final String USER_NAME = "UserName";
    private static final String F_NAME = "FirstName";
    private static final String L_NAME = "LastName";
    public static final String MOBILE_NO = "MobileNo";
    public static final String EMAIL_ID = "EmailId";
    public static final String COMPANY_NAME = "CompanyName";
    public static final String Is_Deleted = "IsDelete";
    public static final String Is_Sync = "IsSync";
    public static final String BADGE = "Badge";
    public static final String COMMENT = "Comment";
    public static final String QUE1 = "Que1";
    public static final String QUE2 = "Que2";
    public static final String OTHER_DATA = "OtherData";
    public static final String PHONE = "phone";
    public static final String PROMOTIONAL = "promotional";
    public static final String SURVEY = "survey";
    public static final String SESSIONID = "sessionid";
    public static final String SESSIONNAME = "sessionname";


    private String ArmBandId;
    private String FirstName;
    private String LastName;
    private String Telephone;
    private String Email;
    private String CompanyName;
    private String CustomerID ;
    private String Title;
    private   String Address1 ;
    private   String Address2 ;
    private   String Address3 ;
    private  String City;
    private  String State ;
    private   String Zip ;
    private  String Country ;
    private  String Phone ;
    private  String Mobile ;
    private  String RegType ;
    private   String Voucher ;
    private  String Comment = "";
    private  String que1 = "";
    private  String que2 = "";
    private  String OtherData = "";
    private boolean IsDeleted;
    private boolean IsSync;

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public boolean isSync() {
        return IsSync;
    }

    public void setSync(boolean sync) {
        IsSync = sync;
    }

    public String getArmBandId() {
        return ArmBandId;
    }
    public void setArmBandId(String armBandId) {
        ArmBandId = armBandId;
    }

    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }
    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }
    public void setEmail(String email) {
        Email = email;
    }

    public String getTelephone() {
        return Telephone;
    }
    public void setTelephone(String telephone) {
        Telephone = telephone;
    }

    public String getCompanyName() {
        return CompanyName;
    }
    public void setCompanyName(String companyNAme) {
        this.CompanyName = companyNAme;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getAddress3() {
        return Address3;
    }

    public void setAddress3(String address3) {
        Address3 = address3;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZip() {
        return Zip;
    }

    public void setZip(String zip) {
        Zip = zip;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getRegType() {
        return RegType;
    }

    public void setRegType(String regType) {
        RegType = regType;
    }

    public String getVoucher() {
        return Voucher;
    }

    public void setVoucher(String voucher) {
        Voucher = voucher;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getQue1() {
        return que1;
    }

    public void setQue1(String que1) {
        this.que1 = que1;
    }

    public String getQue2() {
        return que2;
    }

    public void setQue2(String que2) {
        this.que2 = que2;
    }

    public String getOtherData() {
        return OtherData;
    }

    public void setOtherData(String otherData) {
        OtherData = otherData;
    }

    @Override
    public String toString() {
        return "Register{" +
                "ArmBandId='" + ArmBandId + '\'' +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", Telephone='" + Telephone + '\'' +
                ", Email='" + Email + '\'' +
                ", CompanyName='" + CompanyName + '\'' +
                ", CustomerID='" + CustomerID + '\'' +
                ", Title='" + Title + '\'' +
                ", Address1='" + Address1 + '\'' +
                ", Address2='" + Address2 + '\'' +
                ", Address3='" + Address3 + '\'' +
                ", City='" + City + '\'' +
                ", State='" + State + '\'' +
                ", Zip='" + Zip + '\'' +
                ", Country='" + Country + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", RegType='" + RegType + '\'' +
                ", Voucher='" + Voucher + '\'' +
                '}';
    }
}
