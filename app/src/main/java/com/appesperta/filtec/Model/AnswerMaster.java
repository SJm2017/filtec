package com.appesperta.filtec.Model;

import android.text.Html;


import com.appesperta.filtec.Application;

import org.json.JSONObject;

import java.io.Serializable;

/** Created by Android-1 on 12/2/2016.*/
public class AnswerMaster implements Serializable {

    public static final String TABLE_NAME = "AnswerMaster";

    public static final String ANSWER_ID = "AnswerId";
    public static final String EVENT_ID = "EventId";
    public static final String QUESTION_ID = "QuestionId";
    public static final String COMPANY_ID = "CompanyId";
    public static final String ANSWER = "Answer";
    public static final String DATE = "ModifiedDate";
    public static final String RESULT_TITLE = "ResultTitle";
    public static final String Asm_Value = "AsmValue";
    public static final String RESULT_Desc = "ResultDesc";
    public static final String ANS_IMAGE= "AnsImage";
    public static final String PIE_DESC= "PieAnswerDescription";
    public static final String IS_ACTIVE = "IsActive";
    public static final String IS_DELETE = "IsDeleted";
    public static final String Display_Order = "DisplayOrder";

    private String AnswerId;
    private String EventId;
    private String QuestionId;
    private String CompanyId;
    private String Answer;
    private String ModifiedDate;
    private String ResultTitle;
    private String AsmValue;
    private String ResultDesc;
    private String AnsImage;
    private String PieAnswerDescription;
    private String IsActive;
    private String IsDeleted;
    private String DisplayOrder;

    public AnswerMaster(){}
    public AnswerMaster(JSONObject j1) {
        try {
            setAnswerId(j1.optString(ANSWER_ID));
            setEventId(Application.EventId);
            setQuestionId(j1.optString(QUESTION_ID));
            setCompanyId(Application.CompanyId);
            setAnswer(Html.fromHtml(j1.optString(ANSWER).trim()).toString());
            setModifiedDate(j1.optString(DATE));
            setResultTitle(Html.fromHtml(j1.optString(RESULT_TITLE).trim()).toString());
            setAsmValue(Html.fromHtml(j1.optString(Asm_Value).trim()).toString());
            setResultDesc(Html.fromHtml(j1.optString(RESULT_Desc).trim()).toString());
            setAnsImage(j1.optString(ANS_IMAGE));
            setPieAnswerDescription(Html.fromHtml(j1.optString(PIE_DESC).trim()).toString());
            setIsActive(j1.optString(IS_ACTIVE));
            setIsDeleted(j1.optString(IS_DELETE));
            setDisplayOrder(j1.optString(Display_Order));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getAnswerId() {
        return AnswerId;
    }
    public void setAnswerId(String answerId) {
        AnswerId = answerId;
    }

    public String getEventId() {
        return EventId;
    }
    public void setEventId(String eventId) {
        EventId = eventId;
    }

    public String getQuestionId() {
        return QuestionId;
    }
    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    public String getCompanyId() {
        return CompanyId;
    }
    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getAnswer() {
        return Answer;
    }
    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }
    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getResultTitle() {
        return ResultTitle;
    }
    public void setResultTitle(String resultTitle) {
        ResultTitle = resultTitle;
    }

    public String getAsmValue() {
        return AsmValue;
    }
    public void setAsmValue(String asmValue) {
        AsmValue = asmValue;
    }

    public String getResultDesc() {
        return ResultDesc;
    }
    public void setResultDesc(String resultDesc) {
        ResultDesc = resultDesc;
    }

    public String getAnsImage() {
        return AnsImage;
    }
    public void setAnsImage(String ansImage) {
        AnsImage = ansImage;
    }

    public String getPieAnswerDescription() {
        return PieAnswerDescription;
    }
    public void setPieAnswerDescription(String pieAnswerDescription) {
        PieAnswerDescription = pieAnswerDescription;
    }


    public String getIsDeleted() {
        return IsDeleted;
    }
    public void setIsDeleted(String isDeleted) {
        IsDeleted = isDeleted;
    }

    public String getIsActive() {
        return IsActive;
    }
    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getDisplayOrder() {
        return DisplayOrder;
    }
    public void setDisplayOrder(String displayOrder) {
        DisplayOrder = displayOrder;
    }

}
