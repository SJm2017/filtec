package com.appesperta.filtec.Model;

public class LogMaster {
    public static final String KEY_TABLE_NAME = "QuestionAnswersLogMaster";
    public static final String KEY_JSON_ARRAY = "lstAnswer";
    public static final String KEY_LOG_ID = "LogId";
    public static final String KEY_QUESTION_ID = "QuestionId";
    public static final String KEY_ASM_VALUE = "ASMValue";
    public static final String KEY_ANSWER = "Answer";
    public static final String KEY_ANSWER_ID = "AnswerId";
    public static final String KEY_ANSWER_IMAGE = "AnswerImage";
    public static final String KEY_COMPANY_ID = "CompanyId";
    public static final String KEY_EVENT_ID = "EventId";
    public static final String KEY_PIE_ANSWER_DESC = "PieAnswerDesc";
    public static final String KEY_RESULT_DESC = "ResultDesc";
    public static final String KEY_RESULT_TITLE = "ResultTitle";
    public static final String KEY_MODIFIED_DATE = "ModifiedDate";
    public static final String KEY_USER_ID = "UserId";
    public static final String KEY_ARMBAND_ID = "ArmBandId";
    public static final String KEY_FLAG = "Flag";
    public static final String KEY_IS_SYNC = "IsSync";
    public static final String KEY_SET_ID = "SetId";
    public static final String KEY_PRODUCT_ID = "ProductId";

    private String questionId = "", asmValue = "", answer = "", answerId = "", answerImage = "", companyId = "", eventId = "", pieAnswerDesc = "", resultDesc = "",
            resultTitle = "", modifiedDate = "", userId = "",ArmBandId="",SetId="",ProductId="";

    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getQuestionId() {
        return questionId;
    }
    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getAsmValue() {
        return asmValue;
    }
    public void setAsmValue(String asmValue) {
        this.asmValue = asmValue;
    }

    public String getAnswer() {
        return answer;
    }
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerId() {
        return answerId;
    }
    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getAnswerImage() {
        return answerImage;
    }
    public void setAnswerImage(String answerImage) {
        this.answerImage = answerImage;
    }

    public String getCompanyId() {
        return companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getEventId() {
        return eventId;
    }
    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getPieAnswerDesc() {
        return pieAnswerDesc;
    }
    public void setPieAnswerDesc(String pieAnswerDesc) {
        this.pieAnswerDesc = pieAnswerDesc;
    }

    public String getResultDesc() {
        return resultDesc;
    }
    public void setResultDesc(String resultDesc) {
        this.resultDesc = resultDesc;
    }

    public String getResultTitle() {
        return resultTitle;
    }
    public void setResultTitle(String resultTitle) {
        this.resultTitle = resultTitle;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }
    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getArmBandId() {
        return ArmBandId;
    }
    public void setArmBandId(String ArmBandId) {
        this.ArmBandId = ArmBandId;
    }

    public String getSetId() {
        return SetId;
    }
    public void setSetId(String SetId) {
        this.SetId = SetId;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    @Override
    public String toString() {
        return "LogMaster{" +
                "questionId='" + questionId + '\'' +
                ", asmValue='" + asmValue + '\'' +
                ", answer='" + answer + '\'' +
                ", answerId='" + answerId + '\'' +
                ", answerImage='" + answerImage + '\'' +
                ", companyId='" + companyId + '\'' +
                ", eventId='" + eventId + '\'' +
                ", pieAnswerDesc='" + pieAnswerDesc + '\'' +
                ", resultDesc='" + resultDesc + '\'' +
                ", resultTitle='" + resultTitle + '\'' +
                ", modifiedDate='" + modifiedDate + '\'' +
                ", userId='" + userId + '\'' +
                ", ArmBandId='" + ArmBandId + '\'' +
                ", SetId='" + SetId + '\'' +
                ", ProductId='" + ProductId + '\'' +
                '}';
    }
}
