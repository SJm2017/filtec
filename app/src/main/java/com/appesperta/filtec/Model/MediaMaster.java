package com.appesperta.filtec.Model;

import android.util.Log;

import org.json.JSONObject;

/*** Created by Android-1 on 1/10/2017.*/

public class MediaMaster {
    public static final String TABLE_NAME = "Mediafiles";

    public static final String QUESTION_ID = "QuestionId";
    public static final String MEDIA_NAME = "MediaName";
    public static final String MEDIA_TYPE = "MediaType";
    public static final String MEDIA_PATH = "MediaPath";

    private String MediaName;
    private String MediaType;
    private String MediaPath;
    private String questionId;

    public MediaMaster() {}

    MediaMaster(JSONObject j, String questionId) {
        try {
            setQuestionId(questionId);
            if (!j.optString(MEDIA_TYPE).equals("video")) {
                final String name = j.optString(MEDIA_PATH).substring(j.optString(MEDIA_PATH).lastIndexOf('/') + 1);
                Log.e("TAG", "MediaMaster: "+name);
                setMediaName(name);
            } else {
                setMediaName(j.optString(MEDIA_NAME));
            }
            setMediaType(j.optString(MEDIA_TYPE));
            setMediaPath(j.optString(MEDIA_PATH));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getQuestionId() {
        return questionId;
    }
    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getMediaPath() {
        return MediaPath;
    }
    public void setMediaPath(String mediaPath) {
        MediaPath = mediaPath;
    }

    public String getMediaType() {
        return MediaType;
    }
    public void setMediaType(String mediaType) {
        MediaType = mediaType;
    }

    public String getMediaName() {
        return MediaName;
    }
    public void setMediaName(String mediaName) {
        MediaName = mediaName;
    }


}
