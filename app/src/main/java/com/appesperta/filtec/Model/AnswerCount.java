package com.appesperta.filtec.Model;

public class AnswerCount {
    private String AnswerId;
    private int AnswerCount;

    public String getAnswerId() {
        return AnswerId;
    }
    public void setAnswerId(String answerId) {
        AnswerId = answerId;
    }

    public int getAnswerCount() {
        return AnswerCount;
    }
    public void setAnswerCount(int answerCount) {
        AnswerCount = answerCount;
    }

}
