package com.appesperta.filtec.Model;

import org.json.JSONObject;

public class ProductMaster {

    public static final String TABLE_NAME = "ProductMaster";
    public static final String PRODUCT_ID = "ProductId";
    public static final String PRODUCT_NAME = "ProductName";
    public static final String PRODUCT_DES = "Description";
    public static final String PRODUCT_IMAGE = "ProductImage";
    public static final String PRODUCT_LINK = "DocumentLink";
    public static final String SET_ID = "SetId";
    public static final String QUESTION_ID = "QuestionId";


    private String ProductId="";
    private String ProductName="";
    private String Description="";
    private String ProductImage="";
    private String DocumentLink="";
    private String setId="";
    private String QuestionId="";

    public ProductMaster() {

    }

    public ProductMaster(JSONObject j) {
        try {
            setProductId(j.optString(PRODUCT_ID));
            setProductName(j.optString(PRODUCT_NAME));
            setDescription(j.optString(PRODUCT_DES));
            setProductImage(j.optString(PRODUCT_IMAGE));
            setDocumentLink(j.optString(PRODUCT_LINK));
            setSetId(j.optString(SET_ID));
            setQuestionId(j.optString(QUESTION_ID));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getProductId() {
        return ProductId;
    }
    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }
    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getDescription() {
        return Description;
    }
    public void setDescription(String description) {
        Description = description;
    }

    public String getProductImage() {
        return ProductImage;
    }
    public void setProductImage(String productImage) {
        ProductImage = productImage;
    }

    public String getDocumentLink() {
        return DocumentLink;
    }
    public void setDocumentLink(String documentLink) {
        DocumentLink = documentLink;
    }

    public String getSetId() {
        return setId;
    }
    public void setSetId(String setId) {
        this.setId = setId;
    }

    public String getQuestionId() {
        return QuestionId;
    }
    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

}
