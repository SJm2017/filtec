package com.appesperta.filtec.Model;

import android.text.Html;
import android.util.Log;

import org.json.JSONObject;

/*Created by Android-1 on 12/2/2016.*/
public class TemplateMaster {
    private static final String TAG = "TemplateMaster";
    public static final String TABLE_NAME = "TemplateMaster";

    public static String MESSAGE_CONTENT = "MessageContent";
    public static String DISPLAY_ORDER = "DisplayOrder";


    private String messageContent;
    private String displayOrder;

    public TemplateMaster() {
    }

    public TemplateMaster(JSONObject j1) {
        Log.e(TAG, "TemplateMaster: " + j1);
        try {
            setMessageContent(Html.fromHtml(j1.optString(MESSAGE_CONTENT).trim()).toString().replaceAll("'","/"));
            setDisplayOrder(j1.optString(DISPLAY_ORDER));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getMessageContent() {
        return messageContent;
    }
    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getDisplayOrder() {
        return displayOrder;
    }
    public void setDisplayOrder(String displayOrder) {
        this.displayOrder = displayOrder;
    }

}