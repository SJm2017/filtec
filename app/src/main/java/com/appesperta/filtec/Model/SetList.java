package com.appesperta.filtec.Model;

import org.json.JSONObject;

/*reated by Jitesh Dalsaniya on 25-Nov-16.*/

public class SetList {

    public static final String TABLE_NAME = "SetList";
    public static final String SET_ID = "SetId";
    public static final String SET_NAME = "SetName";

    private String SetId, SetName;

    public SetList(JSONObject j) {
        try {
            setSetId(j.optString("SetId"));
            setSetName(j.optString("SetName"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getSetId() {
        return SetId;
    }
    private void setSetId(String setId) {
        SetId = setId;
    }

    public String getSetName() {
        return SetName;
    }
    private void setSetName(String setName) {
        SetName = setName;
    }

}
