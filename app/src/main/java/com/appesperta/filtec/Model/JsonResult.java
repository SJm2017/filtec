package com.appesperta.filtec.Model;

/*Created by APPESPERTA-1 on 16/02/2018.*/

import java.util.ArrayList;
import java.util.List;

public class JsonResult {
    private String QuestionId;
    private int totalCount;
    private int totalAns;
    private List<AnswerCount> answerCountsList = new ArrayList<>();

    public String getQuestionId() {
        return QuestionId;
    }
    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    public int getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalAns() {
        return totalAns;
    }
    public void setTotalAns(int totalAns) {
        this.totalAns = totalAns;
    }

    public List<AnswerCount> getAnswerCountsList() {
        return answerCountsList;
    }
    public void setAnswerCountsList(List<AnswerCount> answerCountsList) {
        this.answerCountsList = answerCountsList;
    }

}


