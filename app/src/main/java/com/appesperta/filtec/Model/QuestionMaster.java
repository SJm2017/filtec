package com.appesperta.filtec.Model;

import android.text.Html;
import android.util.Log;


import com.appesperta.filtec.Application;
import com.appesperta.filtec.Database.LocalDatabase;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

/*** Created by Android-1 on 12/2/2016.*/

public class QuestionMaster implements Serializable {

    public static final String TABLE_NAME = "QuestionMaster";

    public static final String QUESTION_ID = "QuestionId";
    public static final String QUESTION = "Question";
    public static final String QUESTION_NO = "QuestionSrNo";
    public static final String DATE = "ModifiedDate";
    public static final String ANSWER_TYPE_ID = "AnswerTypeId";
    public static final String ANSWER_TYPE = "AnswerType";
    public static final String COMMUNITY_ID = "CommunityResultDisplayId";
    public static final String COMMUNITY = "CommunityResultDisplay";
    public static final String EVENT_ID = "EventId";
    public static final String COMPANY_ID = "CompanyId";
    public static final String IS_ACTIVE = "IsActive";
    public static final String IS_DELETE = "IsDeleted";
    public static final String Set_Id = "SetId";
    public static final String INTERVAL = "Interval";

    private String QuestionId;
    private String Question;
    private String QuestionSrNo;
    private String ModifiedDate;
    private String AnswerTypeId;
    private String AnswerType;
    private String CommunityResultDisplayId;
    private String CommunityResultDisplay;
    private String EventId;
    private String CompanyId;
    private String IsActive;
    private String IsDeleted;
    private String SetId;
    private String Interval;

    public QuestionMaster() {}

    public QuestionMaster(JSONObject j, LocalDatabase db) {
        try {
            setQuestionId(j.optString(QUESTION_ID));
            setQuestion(Html.fromHtml(j.optString(QUESTION).trim()).toString());
            setQuestionSrNo(j.optString(QUESTION_NO));
            setModifiedDate(j.optString(DATE));
            setAnswerTypeId(j.optString(ANSWER_TYPE_ID));
            setAnswerType(Html.fromHtml(j.optString(ANSWER_TYPE).trim()).toString());
            setCommunityResultDisplayId(j.optString(COMMUNITY_ID));
            setCommunityResultDisplay(Html.fromHtml(j.optString(COMMUNITY).trim()).toString());
            setEventId(Application.EventId);
            setCompanyId(Application.CompanyId);
            setIsActive(j.optString(IS_ACTIVE));
            setIsDeleted(j.optString(IS_DELETE));
            setSetId(j.optString(Set_Id));
            setInterval(j.optString(INTERVAL));

            JSONArray js_m = j.getJSONArray("Mediafiles");
            Log.e("TAG", "QuestionMaster: "+js_m );
            for (int i = 0; i < js_m.length() ; i++) {
                JSONObject j1 = js_m.getJSONObject(i);
                MediaMaster mediaMaster = new MediaMaster(j1,j.optString(Set_Id));
                //db.addMediaMaster(mediaMaster);
            }
            JSONArray js = j.getJSONArray("AllAnswerList");
            Log.e("TAG", "QuestionMaster: "+js);
            for (int i = 0; i < js.length() ; i++) {
                JSONObject j1 = js.getJSONObject(i);
                AnswerMaster answerMaster = new AnswerMaster(j1);
                db.addAnswerMaster(answerMaster);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getQuestionId() {
        return QuestionId;
    }
    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    public String getQuestion() {
        return Question;
    }
    public void setQuestion(String question) {
        Question = question;
    }

    public String getQuestionSrNo() {
        return QuestionSrNo;
    }
    public void setQuestionSrNo(String questionSrNo) {
        QuestionSrNo = questionSrNo;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }
    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getAnswerTypeId() {
        return AnswerTypeId;
    }
    public void setAnswerTypeId(String answerTypeId) {
        AnswerTypeId = answerTypeId;
    }

    public String getAnswerType() {
        return AnswerType;
    }
    public void setAnswerType(String answerType) {
        AnswerType = answerType;
    }

    public String getCommunityResultDisplayId() {
        return CommunityResultDisplayId;
    }
    public void setCommunityResultDisplayId(String communityResultDisplayId) {
        CommunityResultDisplayId = communityResultDisplayId;
    }

    public String getCommunityResultDisplay() {
        return CommunityResultDisplay;
    }
    public void setCommunityResultDisplay(String communityResultDisplay) {
        CommunityResultDisplay = communityResultDisplay;
    }

    public String getEventId() {
        return EventId;
    }
    public void setEventId(String eventId) {
        EventId = eventId;
    }

    public String getCompanyId() {
        return CompanyId;
    }
    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }


    public String getIsDeleted() {
        return IsDeleted;
    }
    public void setIsDeleted(String isDeleted) {
        IsDeleted = isDeleted;
    }

    public String getIsActive() {
        return IsActive;
    }
    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getSetId() {
        return SetId;
    }
    public void setSetId(String setId) {
        SetId = setId;
    }

    public String getInterval() {
        return Interval;
    }
    public void setInterval(String interval) {
        Interval = interval;
    }


    @Override
    public String toString() {
        return "QuestionMaster{" +
                "QuestionId='" + QuestionId + '\'' +
                ", Question='" + Question + '\'' +
                ", QuestionSrNo='" + QuestionSrNo + '\'' +
                ", ModifiedDate='" + ModifiedDate + '\'' +
                ", AnswerTypeId='" + AnswerTypeId + '\'' +
                ", AnswerType='" + AnswerType + '\'' +
                ", CommunityResultDisplayId='" + CommunityResultDisplayId + '\'' +
                ", CommunityResultDisplay='" + CommunityResultDisplay + '\'' +
                ", EventId='" + EventId + '\'' +
                ", CompanyId='" + CompanyId + '\'' +
                '}';
    }
}
