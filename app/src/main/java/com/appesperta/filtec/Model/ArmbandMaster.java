package com.appesperta.filtec.Model;

/*** Created by Android-1 on 12/2/2016.*/
public class ArmbandMaster {

    public static final String TABLE_NAME = "TblArmbandRegistration";

    public static final String ARM_BAND_ID = "ArmBandID";
    public static final String USER_ID = "UserID";
    public static final String USER_NAME = "UserName";
    private static final String F_NAME = "FirstName";
    private static final String L_NAME = "LastName";
    public static final String MOBILE_NO = "MobileNo";
    public static final String EMAIL_ID = "EmailId";
    public static final String COMPANY_NAME = "CompanyName";
    public static final String Is_Deleted = "IsDelete";
    public static final String Is_Sync = "IsSync";
    public static final String BADGE = "Badge";
    public static final String COMMENT = "Comment";
    public static final String QUE1 = "Que1";
    public static final String QUE2 = "Que2";
    public static final String OTHER_DATA = "OtherData";
    public static final String PHONE = "phone";
    public static final String PROMOTIONAL = "promotional";
    public static final String SURVEY = "survey";
    public static final String SESSIONID = "sessionid";
    public static final String SESSIONNAME = "sessionname";

    private String ArmBandID;
    private String UserID;
    private String UserName;
    private String MobileNo;
    private String EmailId;
    private String CompanyName;
    private String Badge = "";
    private boolean IsDeleted;
    private boolean IsSync;
    private String Comment = "";
    private String que1 = "";
    private String que2 = "";
    private String OtherData = "";
    private String OptPromotional = "";
    private String OptSurvey = "";
    private String PhoneNo = "";
    private String sessionname = "";
    private String Sessioid = "";


    public ArmbandMaster() {
    }

    /*public ArmbandMaster(JSONObject j, String id) {
        try {
            setArmBandID(id);
            setUserID(j.optString(USER_ID));
            setUserName(Html.fromHtml(j.optString(F_NAME).trim()).toString()+" "+Html.fromHtml(j.optString(L_NAME).trim()).toString());
            setMobileNo(j.optString(MOBILE_NO));
            setEmailId(j.optString(EMAIL_ID));
            setCompanyName(j.optString(COMPANY_NAME));
            setDeleted(false);
            setSync(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public String getOptPromotional() {
        return OptPromotional;
    }

    public void setOptPromotional(String optPromotional) {
        OptPromotional = optPromotional;
    }

    public String getOptSurvey() {
        return OptSurvey;
    }

    public void setOptSurvey(String optSurvey) {
        OptSurvey = optSurvey;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getArmBandID() {
        return ArmBandID;
    }

    public void setArmBandID(String armBandID) {
        ArmBandID = armBandID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getBadge() {
        return Badge;
    }

    public void setBadge(String badge) {
        Badge = badge;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public boolean isSync() {
        return IsSync;
    }

    public void setSync(boolean sync) {
        IsSync = sync;
    }

    public String getQue1() {
        return que1;
    }

    public void setQue1(String que1) {
        this.que1 = que1;
    }

    public String getQue2() {
        return que2;
    }

    public void setQue2(String que2) {
        this.que2 = que2;
    }

    public String getOtherData() {
        return OtherData;
    }

    public void setOtherData(String otherData) {
        OtherData = otherData;
    }

    public String getSessionname() {
        return sessionname;
    }

    public void setSessionname(String sessionname) {
        this.sessionname = sessionname;
    }

    public String getSessioid() {
        return Sessioid;
    }

    public void setSessioid(String sessioid) {
        Sessioid = sessioid;
    }
}
