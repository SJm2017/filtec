package com.appesperta.filtec.Model;

/*Created by Jitesh Dalsaniya on 25-Nov-16.*/

public class PercentageGraph {

    public static final String ARRAY = "QuestionPerCountList";
    public static final String PERCENTAGE = "Percentage";
    public static final String ANSWER = "Answer";
    public static final String PIE_ANSWER_DESCRIPTION = "PieAnswerDesc";

    private float percentage;
    private String answer, description;

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
