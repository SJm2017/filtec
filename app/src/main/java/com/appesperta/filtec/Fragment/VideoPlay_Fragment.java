package com.appesperta.filtec.Fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.appesperta.filtec.QueAns.QueAnsPresenterImpl;
import com.appesperta.filtec.R;
import com.bumptech.glide.Glide;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoPlay_Fragment extends Fragment {

    private static final String TAG = "VideoPlay_Fragment";
    @BindView(R.id.img_product)
    ImageView img_product;
    QueAnsPresenterImpl queAnsPresenter;
    String productId;

    public static VideoPlay_Fragment newInstance(QueAnsPresenterImpl queAnsPresenter, String productId) {
        VideoPlay_Fragment videoPlay_new_fragment = new VideoPlay_Fragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("Presenter", queAnsPresenter);
        bundle.putString("productId", productId);
        videoPlay_new_fragment.setArguments(bundle);
        return videoPlay_new_fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_video, container, false);
        ButterKnife.bind(this, v);
        Bundle bundle = getArguments();
        if (bundle != null) {
            queAnsPresenter = bundle.getParcelable("Presenter");
            productId = bundle.getString("productId");
        }
       // image = queAnsPresenter.getVideo(productId);
        File file = new File(getActivity().getCacheDir() + "/image/" + Uri.parse(queAnsPresenter.getVideo(productId)).getLastPathSegment());
        Log.e(TAG, "onCreateView: image "+file);
        if (file.exists()) {
            Glide.with(getActivity())
//                    .load(file)
                    .load(R.drawable.survey)
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .error(R.drawable.ic_launcher_foreground)
                    .into(img_product);
        } else {
            Glide.with(getActivity())
                    .load(R.drawable.survey)
//                    .load(queAnsPresenter.getVideo(productId))
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .error(R.drawable.ic_launcher_foreground)
                    .into(img_product);
        }

        return v;
    }
}
