package com.appesperta.filtec.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.TextView;

import com.appesperta.filtec.Application;
import com.appesperta.filtec.Model.AnswerMaster;
import com.appesperta.filtec.Model.QuestionMaster;
import com.appesperta.filtec.QueAns.QueAnsPresenterImpl;
import com.appesperta.filtec.R;
import com.appesperta.filtec.utils.Wrapper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Question_Fragment extends Fragment {

    private static final String TAG = "Question_Fragment";

    @BindView(R.id.txt_que)
    TextView txt_que;
    @BindView(R.id.grid_ans)
    GridView grid_ans;
    @BindView(R.id.btn_go)
    Button btn_go;


    QueAnsPresenterImpl queAnsPresenter;
    QuestionMaster questionMaster;
    List<AnswerMaster> answerMasterList;
    List<Boolean> isChecked = new ArrayList<>();
    public static String ans = "";
    boolean isEnable = false;
    AnswerListAdapter adapter;
    String productId;
    public static Question_Fragment newInstance(QueAnsPresenterImpl queAnsPresenter, QuestionMaster questionMaster, List<AnswerMaster> answerMasterList,String productId) {
        Question_Fragment question_fragment = new Question_Fragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("Question", questionMaster);
        bundle.putSerializable("Answers", new Wrapper(answerMasterList));
        bundle.putParcelable("Presenter", queAnsPresenter);
        bundle.putString("productId", productId);
        question_fragment.setArguments(bundle);
        return question_fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_question, container, false);
        ButterKnife.bind(this, v);
        Bundle bundle = getArguments();
        if (bundle != null) {
            questionMaster = (QuestionMaster) bundle.getSerializable("Question");
            queAnsPresenter = bundle.getParcelable("Presenter");
            productId = bundle.getString("productId");
            Wrapper wrapper = (Wrapper) bundle.getSerializable("Answers");
            if (wrapper != null)
                answerMasterList = wrapper.getAnswerMasterList();

        }

        btn_go.setText(getResources().getString(R.string.login));
        btn_go.setTextColor(Color.parseColor(Application.themeColor));
        btn_go.setBackground(Application.rectangle_btn(Application.textColor));
        btn_go.setPadding(35, 7, 35, 7);
        btn_go.setTextSize(TypedValue.COMPLEX_UNIT_PX, (20 * Application.dip));

        if (questionMaster != null) {
            txt_que.setText(questionMaster.getQuestion());
            Log.e(TAG, "onCreateView: answerMasterList :: " + answerMasterList.size());
            if (answerMasterList.size() == 8) {
                txt_que.setTextSize(TypedValue.COMPLEX_UNIT_PX, (20 * Application.dip));
            } else {
                txt_que.setTextSize(TypedValue.COMPLEX_UNIT_PX, (22 * Application.dip));
            }
            for (int i = 0; i < answerMasterList.size(); i++) {
                isChecked.add(false);
            }
            adapter = new AnswerListAdapter();
            grid_ans.setAdapter(adapter);
        }

        return v;
    }

    @OnClick(R.id.btn_go)
    public void onCLickGo() {
        Log.e(TAG, "onCLickGo: isChecked " + isChecked);
        giveAnswer();
    }

    public void giveAnswer() {
        isEnable = true;
        adapter.notifyDataSetChanged();
        List<String> answerIdList = new ArrayList<>();
        for (int i = 0; i < isChecked.size(); i++) {
            if (isChecked.get(i)) {
                if (!answerMasterList.get(i).getAnswer().equalsIgnoreCase("all the above")) {
                    answerIdList.add(answerMasterList.get(i).getAnswerId());
                    ans = answerMasterList.get(i).getAnswer() + ",";
                }
            }
        }
        btn_go.setVisibility(View.INVISIBLE);
        Answer_Fragment.interval = questionMaster.getInterval();
        queAnsPresenter.GiveAnswer(questionMaster.getQuestionId(), answerIdList, questionMaster.getCommunityResultDisplay(), answerMasterList,productId);
    }

    private class AnswerListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return answerMasterList.size();
        }

        @Override
        public AnswerMaster getItem(int i) {
            return answerMasterList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            CheckBox ch = new CheckBox(getActivity());
            ch.setText(answerMasterList.get(i).getAnswer());
            if (answerMasterList.size() == 4) {
                ch.setButtonDrawable(Application.makeSelector_new(55, 55));
                ch.setTextSize(TypedValue.COMPLEX_UNIT_PX, (15 * Application.dip));
                ch.setLines(4);
                ch.setEllipsize(TextUtils.TruncateAt.END);
            }

            if (answerMasterList.size() == 8) {
                ch.setButtonDrawable(Application.makeSelector_new(35, 35));
                ch.setTextSize(TypedValue.COMPLEX_UNIT_PX, (12 * Application.dip));
                ch.setLines(2);
                ch.setEllipsize(TextUtils.TruncateAt.END);
            }
            ch.setTextColor(Color.parseColor(Application.textColor));
            ch.setPadding((int) Application.dpToPx(15), (int) Application.dpToPx(15), (int) Application.dpToPx(15), (int) Application.dpToPx(15));
            ch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (answerMasterList.get(i).getAnswer().equalsIgnoreCase("all the above")) {
                        for (int j = 0; j < isChecked.size(); j++) {
                            isChecked.set(j, b);
                        }
                        notifyDataSetChanged();
                    } else {
                        isChecked.set(i, b);
                    }
                }
            });
            ch.setChecked(isChecked.get(i));
            if (isEnable) {
                ch.setEnabled(false);
            } else {
                ch.setEnabled(true);
            }
            return ch;
        }
    }
}
