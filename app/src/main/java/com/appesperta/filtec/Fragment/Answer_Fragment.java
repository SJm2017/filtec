package com.appesperta.filtec.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appesperta.filtec.Application;
import com.appesperta.filtec.Database.LocalDatabase;
import com.appesperta.filtec.Model.AnswerMaster;
import com.appesperta.filtec.Model.PercentageGraph;
import com.appesperta.filtec.ProductActivity;
import com.appesperta.filtec.QueAns.QueAnsPresenterImpl;
import com.appesperta.filtec.R;
import com.appesperta.filtec.ThankYouActivity;
import com.appesperta.filtec.utils.PieHelper;
import com.appesperta.filtec.utils.PieView;
import com.appesperta.filtec.utils.Wrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Answer_Fragment extends Fragment {

    private static final String TAG = "Answer_Fragment";
    @BindView(R.id.text) TextView text;
    @BindView(R.id.txt_per) TextView txt_per;
    @BindView(R.id.txt_desc) TextView txt_desc;
    @BindView(R.id.pie_view) PieView pie_view;

    QueAnsPresenterImpl queAnsPresenter;
    String questionId = "";
    String title = "";
    public static String interval ="";
    List<PercentageGraph> percentageGraphList = new ArrayList<>();
    List<AnswerMaster> answerMasterList;
    private String[] color = {"#CEC8BA", "#E6E7E8", "#EDECEA", "#DEDCD9", "#C9C5C1", "#CEC5B0", "#CAC4BB", "#E2D4B9"};
    LocalDatabase DB;

    public static Answer_Fragment newInstance(QueAnsPresenterImpl queAnsPresenter, String questionId, List<AnswerMaster> answerMasterList) {
        Answer_Fragment answer_fragment = new Answer_Fragment();
        Bundle bundle = new Bundle();
        bundle.putString("questionId", questionId);
        bundle.putParcelable("Presenter", queAnsPresenter);
        bundle.putSerializable("Answers", new Wrapper(answerMasterList));
        answer_fragment.setArguments(bundle);
        return answer_fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_answer, container, false);
        ButterKnife.bind(this, v);
        DB = new LocalDatabase(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null) {
            questionId = bundle.getString("questionId");
            queAnsPresenter = bundle.getParcelable("Presenter");
            Wrapper wrapper = (Wrapper) bundle.getSerializable("Answers");
            if (wrapper != null)
                answerMasterList = wrapper.getAnswerMasterList();
        }

        text.setTextSize(TypedValue.COMPLEX_UNIT_PX, (17 * Application.dip));
        text.setTextColor(Color.parseColor(Application.textColor));
        title = "<html><body><p align=\"justify\">" + getActivity().getResources().getString(R.string.new_title) + "</p></body></html>";
        text.setText(Html.fromHtml(title));


        percentageGraphList.clear();
        List<Float> maxValue = new ArrayList<>();
        List<String> decs = new ArrayList<>();
        for (int i = 0; i < answerMasterList.size(); i++) {
            PercentageGraph percentageGraph = new PercentageGraph();
            percentageGraph.setDescription(DB.getPieDesc(answerMasterList.get(i).getAnswerId()));
            float f = Float.valueOf(DB.getCommunityResult(questionId, answerMasterList.get(i).getAnswerId()));
            percentageGraph.setPercentage(f);
            if (f == 0){
                percentageGraph.setAnswer("");
            } else {
                percentageGraph.setAnswer(answerMasterList.get(i).getAnswer());
            }
            maxValue.add(f);
            decs.add(answerMasterList.get(i).getResultDesc());
            percentageGraphList.add(percentageGraph);
        }
        Log.e(TAG, "onCreateView maxValue : "+maxValue);
        float max = Collections.max(maxValue);
        String yourAns = "";
        for (int i = 0; i < percentageGraphList.size(); i++) {
            if (percentageGraphList.get(i).getAnswer().equals(Question_Fragment.ans)) {
                yourAns = percentageGraphList.get(i).getPercentage() + "%";
                break;
            }
        }
        for (int i = 0; i < maxValue.size(); i++) {
            if (maxValue.get(i) == max) {
                txt_desc.setText(decs.get(i).replace("{Selected}", "").replace("{TopResult}", ""));
                String s = max + "%";
                if (decs.get(i).contains("{Selected}")) {
                    txt_per.setText(yourAns);
                } else {
                    txt_per.setText(s);
                }
                break;
            }
        }

        ArrayList<PieHelper> pieHelperArrayList = new ArrayList<>();
        for (int i = 0; i < percentageGraphList.size(); i++) {
            String subStr;
            if (percentageGraphList.get(i).getAnswer().length()>21) {
                subStr = percentageGraphList.get(i).getAnswer().substring(0, 22) + "...";
            } else {
                subStr = percentageGraphList.get(i).getAnswer();
            }
            pieHelperArrayList.add(new PieHelper(percentageGraphList.get(i).getPercentage(), subStr, Color.parseColor(color[i])));
        }
        pie_view.setDate(pieHelperArrayList);
        pie_view.showPercentLabel(false);
        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
        finishTimer.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (TextUtils.isEmpty(interval)) {
            Log.e(TAG, "onResume: "+interval);
        }
        finishTimer.start();
    }

    private CountDownTimer finishTimer = new CountDownTimer(Integer.parseInt(interval)*1000, 1000) {
        @Override
        public void onTick(long l) {
            Log.e(TAG, "finishTimer l : " + l);
        }

        @Override
        public void onFinish() {
            this.cancel();
            interval ="";
           /* Intent i = new Intent(getActivity(), ProductActivity.class);
            getActivity().startActivity(i);
            getActivity().finish();*/

            Intent i = new Intent(getActivity(), ThankYouActivity.class);
            getActivity().startActivity(i);
            getActivity().finish();
        }
    };
}
