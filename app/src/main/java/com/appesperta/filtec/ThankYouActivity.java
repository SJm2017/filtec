package com.appesperta.filtec;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class ThankYouActivity extends AppCompatActivity {

    private static final String TAG = "ThankYouActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        finishTimer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finishTimer.cancel();
    }


    @Override
    public void onBackPressed() {}
    private CountDownTimer finishTimer = new CountDownTimer(6 * 1000, 1000) {
        @Override
        public void onTick(long l) {
            Log.e(TAG, "finishTimer l : " + l);
        }

        @Override
        public void onFinish() {
            this.cancel();
            Intent i = new Intent(ThankYouActivity.this, ProductActivity.class);
            startActivity(i);
            finish();
        }
    };
}
