package com.appesperta.filtec.Product;

interface ProductPresenter {

   void getProduct();
   void GetArmBandDetails(String badge);
}
