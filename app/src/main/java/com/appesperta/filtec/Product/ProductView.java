package com.appesperta.filtec.Product;

import com.appesperta.filtec.Model.ProductMaster;

import java.util.List;

public interface ProductView {
    void SetView(List<ProductMaster> productMastersList);
    void showProgressDialog();
    void hideProgressDialog();
    void showSnackBar(String resId);
    void navigateToHome();
    void navigateToRegistration();
}
