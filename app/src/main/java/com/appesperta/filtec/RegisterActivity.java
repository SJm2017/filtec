package com.appesperta.filtec;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.appesperta.filtec.Database.LocalDatabase;
import com.appesperta.filtec.Model.ArmbandMaster;
import com.appesperta.filtec.Model.Register;
import com.appesperta.filtec.RegModel.RegPresenterImpl;
import com.appesperta.filtec.RegModel.RegView;
import com.appesperta.filtec.utils.InternetConnection;
import com.appyvet.rangebar.RangeBar;

import java.lang.ref.WeakReference;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity implements RegView {


    @BindView(R.id.view_logout)
    ImageView view_logout;

    @BindView(R.id.rd_1)
    CheckBox rd_1;
    @BindView(R.id.rd_2)
    CheckBox rd_2;
    @BindView(R.id.rd_3)
    CheckBox rd_3;
    @BindView(R.id.rd_4)
    CheckBox rd_4;

    @BindView(R.id.ch_optsurvey)
    CheckBox ch_optsurvey;

    @BindView(R.id.rg_ans)
    RadioGroup rg_ans;
    @BindView(R.id.rb_y)
    RadioButton rd_yes;
    @BindView(R.id.rb_n)
    RadioButton rd_no;


    @BindView(R.id.layoutSlider)
    LinearLayout layoutSlider;
    @BindView(R.id.txt_rang1)
    TextView txt_rang1;
    @BindView(R.id.txt_rang2)
    TextView txt_rang2;
    @BindView(R.id.txt_rang3)
    TextView txt_rang3;
    @BindView(R.id.txt_rang4)
    TextView txt_rang4;
    @BindView(R.id.txt_rang5)
    TextView txt_rang5;
    @BindView(R.id.rangeBar1)
    RangeBar rangeBar1;
    @BindView(R.id.edit_Fname)
    EditText edit_Fname;
    @BindView(R.id.edit_Lname)
    EditText edit_Lname;
    @BindView(R.id.edit_company)
    EditText edit_company;
    @BindView(R.id.edit_phone)
    EditText edit_phone;
    @BindView(R.id.edit_mobile)
    EditText edit_mobile;
    @BindView(R.id.edit_comment)
    EditText edit_comment;
    @BindView(R.id.edit_email)
    EditText edit_email;
    @BindView(R.id.img_go)
    Button img_go;
    private static final String TAG = "RegisterActivity";
    String BadgeID = "", CustomerID = "";
    LocalDatabase DB;
    ArmbandMaster armbandMaster;
    String data = "";
    String Productid = "";
    String QuestionId = "";
    String StrAnser1 = "";
    String StrAnser2 = "";
    String StrAnser3 = "";
    ProgressDialog pd;
    @BindView(R.id.lin_root)
    LinearLayout lin_root;
    @BindView(R.id.lin1)
    LinearLayout lin1;
RegPresenterImpl regPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        regPresenter = new RegPresenterImpl(this,this);
        lin_root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HideKyeoboard(view);
            }
        });
        lin1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HideKyeoboard(view);
            }
        });
        view_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HideKyeoboard(view);
            }
        });


        data = getIntent().getStringExtra("data");
        try {
            Productid = getIntent().getStringExtra("ProductId");
            QuestionId = getIntent().getStringExtra("QuestionId");
        } catch (Exception e) {

        }
        DB = new LocalDatabase(RegisterActivity.this);
        Log.e(TAG, "onCreate: " + data);
        data = data.replace("^^", "^  ^");
        data = data.replace("^^", "^  ^");
        data = data.replace("^^", "^  ^");
        Log.e(TAG, "onCreate: " + data);
        StringTokenizer st = new StringTokenizer(data, "^");
        //String str[] = data.split("^");
        BadgeID = st.nextToken();
        CustomerID = st.nextToken();
        String FirstName = st.nextToken();
        String LastName = st.nextToken();
        String Title = st.nextToken();
        String Company = st.nextToken();
        String Address1 = st.nextToken();
        String Address2 = st.nextToken();
        String Address3 = st.nextToken();
        String City = st.nextToken();
        String State = st.nextToken();
        String Zip = st.nextToken();
        String Country = st.nextToken();
        String Phone = st.nextToken();
        String Mobile = st.nextToken();
        String Email = st.nextToken();

        String RegType = st.nextToken();

        String Voucher ="";
        try {
            Voucher= st.nextToken();
        }catch (Exception e)
        {}

        edit_Fname.setText(FirstName);
        edit_Lname.setText(LastName);
        edit_company.setText(Company);
        edit_email.setText(Email);
        edit_mobile.setText(Mobile);
        edit_phone.setText(Phone);
        rg_ans.clearCheck();


        StrAnser1 = "";
        StrAnser2 = "";
        StrAnser3 = "";


        rg_ans.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rb_y) {
                    StrAnser3 = "Yes";
                } else if (checkedId == R.id.rb_n) {
                    StrAnser3 = "No";
                }
            }
        });
        rangeBar1.setPinColor(Color.parseColor(Application.themeColor));
        rangeBar1.setSelectorColor(Color.parseColor(Application.themeColor));
        rangeBar1.setConnectingLineColor(Color.parseColor(Application.themeColor));
        rangeBar1.setTickColor(Color.parseColor(Application.themeColor));
        rangeBar1.setBarColor(Color.parseColor(Application.themeColor));
        rangeBar1.setRangeBarEnabled(false);
        rangeBar1.setSeekPinByIndex(0);

        edit_Fname.setBackground(Application.rectangle(RegisterActivity.this));
        edit_Lname.setBackground(Application.rectangle(RegisterActivity.this));
        edit_company.setBackground(Application.rectangle(RegisterActivity.this));
        edit_email.setBackground(Application.rectangle(RegisterActivity.this));
        edit_phone.setBackground(Application.rectangle(RegisterActivity.this));
        edit_mobile.setBackground(Application.rectangle(RegisterActivity.this));

        img_go.setTextColor(Color.parseColor(Application.textColor));
        img_go.setBackground(Application.rectangle_btn(Application.themeColor));
        img_go.setPadding(35, 7, 35, 7);
        img_go.setTextSize(TypedValue.COMPLEX_UNIT_PX, (20 * Application.dip));
        img_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (!edit_mobile.getText().toString().isEmpty() && (edit_mobile.getText().toString().trim().length() < 10 || edit_mobile.getText().toString().trim().length() > 13)) {
//                    Toast.makeText(getApplicationContext(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
//                    return;
//                }

                if (edit_email.getText().toString().length() > 0) {
                    if (!isEmailValid(edit_email.getText().toString().trim())) {
                        Toast.makeText(getApplicationContext(), "Enter valid emailid", Toast.LENGTH_SHORT).show();
                        edit_email.requestFocus();
                        return;
                    }
                }
                String[] s = {"0-3 months", "3-6 months",
                        "6-9 months",
                        "9-12 months",
                        "12+ months"};

                StrAnser2 = "";
                if (rd_1.isChecked()) {
                    if (StrAnser2.length() > 0)
                        StrAnser2 = StrAnser2 + "||";

                    StrAnser2 = StrAnser2 + "Fill level";
                }
                if (rd_2.isChecked()) {
                    if (StrAnser2.length() > 0)
                        StrAnser2 = StrAnser2 + "||";

                    StrAnser2 = StrAnser2 + "Empty Bottle Inspection";
                }
                if (rd_3.isChecked()) {
                    if (StrAnser2.length() > 0)
                        StrAnser2 = StrAnser2 + "||";

                    StrAnser2 = StrAnser2 + "Case Inspection";
                }
                if (rd_4.isChecked()) {
                    if (StrAnser2.length() > 0)
                        StrAnser2 = StrAnser2 + "||";

                    StrAnser2 = StrAnser2 + "Vision Inspection";
                }

                Log.e(TAG, "onClick:  StrAnser2 : " + StrAnser2);
                StrAnser1 = s[rangeBar1.getRightIndex()];
                armbandMaster = new ArmbandMaster();
                armbandMaster.setBadge(BadgeID);
                armbandMaster.setArmBandID(BadgeID);
                armbandMaster.setCompanyName(edit_company.getText().toString());
                armbandMaster.setEmailId(edit_email.getText().toString());
                armbandMaster.setMobileNo(edit_mobile.getText().toString());
                armbandMaster.setPhoneNo(edit_phone.getText().toString());
                armbandMaster.setUserName(edit_Fname.getText().toString() + " " + edit_Lname.getText().toString());
                armbandMaster.setDeleted(false);
                armbandMaster.setSync(false);
                armbandMaster.setComment(edit_comment.getText().toString());
                armbandMaster.setOtherData(data);
                armbandMaster.setOptSurvey(ch_optsurvey.isChecked() + "");
                armbandMaster.setQue1(StrAnser1);
                armbandMaster.setQue2(StrAnser2);
                armbandMaster.setOptPromotional(StrAnser3);

                if (Productid != null && Productid.length() > 0 && InternetConnection.checkConnection(getApplicationContext())) {
                    regPresenter.saveRegistraion(armbandMaster,Productid,edit_comment.getText().toString());
                } else {
                    DB.addArmbandMaster(armbandMaster);
                    if (!Application.Sync_r) {
                        Application.getInstant().getThread_r().start();
                    }
                    Toast.makeText(getApplicationContext(), "Registered successfully.", Toast.LENGTH_SHORT).show();
                    finish();
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void HideKyeoboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void showProgressDialog() {
            pd = new ProgressDialog(RegisterActivity.this);
            pd.setMessage(getString(R.string.please_wait));
            pd.setIndeterminate(false);
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.show();

    }

    @Override
    public void hideProgressDialog() {
        try {
            if (pd != null && pd.isShowing()) {
                pd.dismiss();
            }
        } catch (final IllegalArgumentException e) {
            // Handle or log or ignore
            e.printStackTrace();
        } catch (final Exception e) {
            // Handle or log or ignore
        } finally {
            pd = null;
        }
    }

    @Override
    public void showSnackBar(String resId) {
        Toast.makeText(getApplicationContext(), resId, Toast.LENGTH_LONG).show();
    }
    @Override
    public void navigateToHome() {

    }

    @Override
    public void navigateToRegistration() {

    }

    @Override
    public void navigateToQuestion(String userid) {
        Intent intent = new Intent(getApplicationContext(),QueAnsActivity.class);
        Log.e(TAG, "navigateToHome: ProductId : "+Productid);
        intent.putExtra("ProductId", Productid);
        intent.putExtra("QuestionId", QuestionId);
        startActivity(intent);
        finish();
    }
}
