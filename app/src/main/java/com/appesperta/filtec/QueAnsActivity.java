package com.appesperta.filtec;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.appesperta.filtec.Fragment.Answer_Fragment;
import com.appesperta.filtec.Fragment.Question_Fragment;
import com.appesperta.filtec.Fragment.VideoPlay_Fragment;
import com.appesperta.filtec.QueAns.QueAnsPresenterImpl;
import com.appesperta.filtec.QueAns.QueAnsView;
import com.appesperta.filtec.Model.AnswerMaster;
import com.appesperta.filtec.Model.QuestionMaster;

import java.util.List;


public class QueAnsActivity extends AppCompatActivity implements QueAnsView{

    QueAnsPresenterImpl queAnsPresenter;
    String ProductId,QuestionId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_que_ans);
        ProductId = getIntent().getExtras().getString("ProductId", "");
        QuestionId = getIntent().getExtras().getString("QuestionId", "");
        queAnsPresenter = new QueAnsPresenterImpl(this,this);
        queAnsPresenter.getQuestion(QuestionId);
        OpenVideoFragment();
    }

    public void replaceFragment1(Fragment fragment, String tag) {
        try {
            Log.e("TAG", "replaceFragment: " + tag);
            FragmentManager fm = getSupportFragmentManager();
            boolean isPopped = fm.popBackStackImmediate(tag, 0);
            if (!isPopped && fm.findFragmentByTag(tag) == null) {
                Fragment currentFragment = fm.findFragmentById(R.id.content_view1);
                if (fragment != currentFragment) {
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.content_view1, fragment, tag);
                    ft.commit();
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void replaceFragment2(Fragment fragment, String tag) {
        try {
            Log.e("TAG", "replaceFragment: " + tag);
            FragmentManager fm = getSupportFragmentManager();
            boolean isPopped = fm.popBackStackImmediate(tag, 0);
            if (!isPopped && fm.findFragmentByTag(tag) == null) {
                Fragment currentFragment = fm.findFragmentById(R.id.content_view2);
                if (fragment != currentFragment) {
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.content_view2, fragment, tag);
                    ft.commit();
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OpenVideoFragment() {
        VideoPlay_Fragment videoPlay_fragment = VideoPlay_Fragment.newInstance(queAnsPresenter,ProductId);
        videoPlay_fragment.setRetainInstance(false);
        replaceFragment1(videoPlay_fragment, VideoPlay_Fragment.class.getSimpleName());
    }

    @Override
    public void OpenQuestionFragment(QuestionMaster questionMaster, List<AnswerMaster> answerMasterList) {
        Question_Fragment question_fragment = Question_Fragment.newInstance(queAnsPresenter,questionMaster,answerMasterList,ProductId);
        question_fragment.setRetainInstance(false);
        replaceFragment2(question_fragment, Question_Fragment.class.getSimpleName());
    }

    @Override
    public void OpenAnswerFragment(String questionId, List<AnswerMaster> answerMasterList) {
        Answer_Fragment answer_fragment = Answer_Fragment.newInstance(queAnsPresenter, questionId, answerMasterList);
        answer_fragment.setRetainInstance(false);
        replaceFragment1(answer_fragment, Answer_Fragment.class.getSimpleName());
    }

}
