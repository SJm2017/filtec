package com.appesperta.filtec.Qrcode;

import android.content.Context;
import android.util.Log;

import com.appesperta.filtec.Application;

import com.appesperta.filtec.Database.LocalDatabase;
import com.appesperta.filtec.Main.MainView;
import com.appesperta.filtec.MainActivity;
import com.appesperta.filtec.Model.ArmbandMaster;
import com.appesperta.filtec.Model.ProductMaster;
import com.appesperta.filtec.Model.QuestionMaster;
import com.appesperta.filtec.Model.SetList;
import com.appesperta.filtec.R;
import com.appesperta.filtec.Retrofit.ApiService;
import com.appesperta.filtec.Retrofit.RetroClient;
import com.appesperta.filtec.utils.InternetConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QrviewPresenterImpl implements QrviewPresenter {

    private static final String TAG = "QrviewPresenterImpl";
    private Context context;
    private QrCodeview mainView;
    private LocalDatabase DB;

    public QrviewPresenterImpl(Context context, QrCodeview mainView) {
        this.context = context;
        this.mainView = mainView;
        DB = new LocalDatabase(context);
    }




    @Override
    public void GetArmBandDetails(final String badge) {

        if (InternetConnection.checkConnection(context)) {
            mainView.showProgressDialog();
            ApiService api = RetroClient.getApiService();
            Call<String> call = api.getArmBandData(Application.CompanyId, Application.EventId, badge);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    parseArmbandResponse(response.body(),badge);
                    mainView.hideProgressDialog();

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    mainView.hideProgressDialog();
                    mainView.showSnackBar(context.getString(R.string.error));
                    Log.e(TAG, "onFailure getSet: " + t.getMessage());
                }
            });
        } else {
            mainView.showSnackBar(context.getString(R.string.error));
        }
    }

    private void parseArmbandResponse(String response,final String badge) {

        try {
            JSONObject jsonObject = new JSONObject(response);
            Log.e(TAG, "getMobile: jsonObject " + jsonObject);
            if (jsonObject.optBoolean("Flag")) {
                ArmbandMaster armbandMaster = new ArmbandMaster();
                String mobile = jsonObject.optString("MobileNo");
                armbandMaster.setMobileNo(mobile);
                armbandMaster.setUserID(jsonObject.optString("UserId"));
                armbandMaster.setUserName(jsonObject.optString("FirstName"));
                // DoRedeem(mobile);
                Application.preferences.setNfcId(badge);
                Application.preferences.setUserId(jsonObject.optString("UserId"));
                mainView.navigateToHome("Welcome " + jsonObject.optString("FirstName") + " " + jsonObject.optString("LastName"));
            } else {
                //User not registered
                mainView.navigateToRegistration();
            }

        } catch (
                Exception e)

        {
            e.printStackTrace();
        }

    }


}
