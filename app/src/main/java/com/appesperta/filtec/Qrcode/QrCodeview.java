package com.appesperta.filtec.Qrcode;

public interface QrCodeview {

    void showProgressDialog();
    void hideProgressDialog();
    void showSnackBar(String resId);
    void navigateToHome(String s);
    void navigateToRegistration();
}
