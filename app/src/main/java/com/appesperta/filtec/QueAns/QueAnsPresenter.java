package com.appesperta.filtec.QueAns;

import com.appesperta.filtec.Model.AnswerMaster;

import java.util.List;

interface QueAnsPresenter {

    void getQuestion(String position);
    void GiveAnswer(String questionId, List<String> answerIdList, String answerType, List<AnswerMaster> answerMasterList,String Productid);
    String getVideo(String setId);
}
