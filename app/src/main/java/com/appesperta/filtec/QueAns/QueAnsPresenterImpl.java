package com.appesperta.filtec.QueAns;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;


import com.appesperta.filtec.Application;
import com.appesperta.filtec.Database.LocalDatabase;
import com.appesperta.filtec.Fragment.Question_Fragment;
import com.appesperta.filtec.Model.AnswerMaster;
import com.appesperta.filtec.Model.LogMaster;
import com.appesperta.filtec.Model.QuestionMaster;

import java.util.List;

/*** Created by Android-1 on 1/7/2017.*/

public class QueAnsPresenterImpl implements QueAnsPresenter,Parcelable {

    private static final String TAG = "NewMainPresenterImpl";
    private QueAnsView queAnsView;
    private LocalDatabase DB;

    public QueAnsPresenterImpl(Context context, QueAnsView queAnsView) {
        this.queAnsView = queAnsView;
        DB = new LocalDatabase(context);
    }

    private QueAnsPresenterImpl(Parcel in) {}
    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
    @Override
    public int describeContents() {
        return 0;
    }
    public static final Creator<QueAnsPresenterImpl> CREATOR = new Creator<QueAnsPresenterImpl>() {
        @Override
        public QueAnsPresenterImpl createFromParcel(Parcel in) {
            return new QueAnsPresenterImpl(in);
        }

        @Override
        public QueAnsPresenterImpl[] newArray(int size) {
            return new QueAnsPresenterImpl[size];
        }
    };

    @Override
    public void getQuestion(String questionId) {
        QuestionMaster questionMaster = DB.getQuestion(questionId);
        List<AnswerMaster> answerMasterList = DB.getAnswerList(questionMaster.getQuestionId());
        Log.e(TAG, "getQuestion: answerMasterList : "+answerMasterList.size() );
        queAnsView.OpenQuestionFragment(questionMaster,answerMasterList);
    }

    @Override
    public void GiveAnswer(String questionId, List<String> answerIdList, String answerType, List<AnswerMaster> answerMasterList,String Productid) {

        for (int i = 0; i <answerIdList.size() ; i++) {
            String resultDesc =DB.getResultDesc(answerIdList.get(i));
            String pieAnswerDesc =DB.getPieAnswerDesc(answerIdList.get(i));
            String ResultTitle =DB.getResultTitle(answerIdList.get(i));
            String AnsImage =DB.getAnsImage(answerIdList.get(i));
            String AsmValue =DB.getAsmValue(answerIdList.get(i));

            LogMaster logMaster = new LogMaster();
            logMaster.setQuestionId(questionId);
            logMaster.setAsmValue(AsmValue);
            logMaster.setAnswer(Question_Fragment.ans);
            logMaster.setAnswerId(answerIdList.get(i));
            logMaster.setAnswerImage(AnsImage);
            logMaster.setCompanyId(Application.CompanyId);
            logMaster.setEventId(Application.EventId);
            logMaster.setPieAnswerDesc(pieAnswerDesc);
            logMaster.setResultDesc(resultDesc);
            logMaster.setResultTitle(ResultTitle);
            logMaster.setModifiedDate("");
            logMaster.setUserId(Application.preferences.getUserId());
            logMaster.setArmBandId(Application.preferences.getNfcId());
            logMaster.setSetId(Application.preferences.getSetId());
            logMaster.setProductId(Productid);
            DB.addLogMaster(logMaster,false);
        }
        queAnsView.OpenAnswerFragment(questionId,answerMasterList);
    }

    @Override
    public String getVideo(String productId) {
        return DB.getImageLink(productId);
    }
}
