package com.appesperta.filtec.QueAns;


import com.appesperta.filtec.Model.AnswerMaster;
import com.appesperta.filtec.Model.QuestionMaster;

import java.util.List;

public interface QueAnsView {
    void OpenVideoFragment();
    void OpenQuestionFragment(QuestionMaster questionMaster, List<AnswerMaster> answerMasterList);
    void OpenAnswerFragment(String questionId, List<AnswerMaster> answerMasterList);
}
