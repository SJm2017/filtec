package com.appesperta.filtec.Retrofit;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {

    @GET("GetSets?")
    Call<String>  getSets(@Query("CompanyId") String companyId,
                          @Query("EventId") String eventId);

    @GET("getfiltecproductlist?")
    Call<String>  getProductList();

    @GET("GetAllQuestionAnswer?")
    Call<String> getAllQuestion(@Query("CompanyId") String companyId,
                                @Query("EventId") String eventId,
                                @Query("LastDate") String date,
                                @Query("SetId") String setId);

    @GET("GetArmBandDetails?")
    Call<String> getArmBandData(@Query("CompanyId") String companyId,
                                @Query("EventId") String eventId,
                                @Query("ArmBandId") String armBandId);

    @GET("SaveArmBand?")
    Call<String> doRegistration(@Query("CompanyId") String companyId,
                                @Query("EventId") String eventId,
                                @Query("ArmBandId") String armBandId,
                                @Query("FName") String fName,
                                @Query("LName") String lName,
                                @Query("MobileNo") String mobileNo,
                                @Query("EmailId") String emailId,
                                @Query("CompanyName") String companyName,
                                @Query("badgeId") String badgeId,
                                @Query("RegisteredFrom") String regfrom);

    @POST("SaveUserInfoFromThirdParty")
    Call<String> SaveUserInfo(@Body RequestBody body);

    @POST("SaveComments")
    Call<String> SaveComments(@Body RequestBody body);

    @GET("GetAllAnswerByQuestionId?")
    Call<String> getAllAnswerByQuestionId(@Query("CompanyId") String companyId,
                                          @Query("EventId") String eventId,
                                          @Query("Date") String date);

    @POST("SaveMultipleQuestionAnswer")
    Call<String> updateSummary(@Body RequestBody body);






    @GET("GetEvents?")
    Call<String> doLogin(@Query("CompanyId") String companyId,
                         @Query("LoginKey") String username);

    @GET("GetAllCompititors?")
    Call<String> getCompetitor(@Query("CompanyId") String companyId,
                               @Query("EventId") String eventId);

    @GET("LeadsAPI.php")
    Call<String> getQRReader(@Query("AuthKey") String authKey,
                             @Query("exid") String ex_id,
                             @Query("eventcode") String eventcode,
                             @Query("function") String function,
                             @Query("badge") String badge,
                             @Query("lastname") String lastname);
    @GET("GetAllBanner")
    Call<String> getBannerImages(@Query("CompanyId") String companyId,
                                 @Query("EventId") String eventId);

    @GET("CheckSurveyCompleted?")
    Call<String> getRedeemData(@Query("CompanyId") String companyId,
                               @Query("EventId") String eventId,
                               @Query("ArmBandId") String armBandId);

    @GET("RedeemPrize?")
    Call<String> getRedeem(@Query("ArmBandId") String armBandId,
                           @Query("isredeem") String isredeem,
                           @Query("EventId") String eventId);

    @GET("GetMessageTemplates")
    Call<String> getTemplates(@Query("CompanyId") String companyId,
                              @Query("EventId") String eventId);

    @POST("SaveSurveyUserNotes?")
    Call<String> updateMessageTemplates(@Query("CompanyId") String companyId,
                                        @Query("EventId") String eventId,
                                        @Query("ArmBandId") String armBandId,
                                        @Query("Notes") String notes);


    @GET("DefaultLatLong?")
    Call<String> CurrentLocation(@Query("Latitude") String lat,
                                 @Query("Longitude") String lng
    );

    @GET("GetSetDetails")
    Call<String> GetSetDetails(@Query("CompanyId") String companyId,
                               @Query("EventId") String eventId);

    @POST("SaveSetDetails")
    Call<String> SaveSetDetails(@Body RequestBody body, @Query("CompanyId") String companyId,
                                @Query("EventId") String eventId);

    @GET("CheckInCheckOutLogin?")
    Call<String> doCheckIn(@Query("CompanyId") String companyId,
                           @Query("UserName") String username);

    @GET("MarkCheckInCheckOut?")
    Call<String> setCheckInOut(@Query("CompanyId") String companyId,
                               @Query("EventId") String armBandId,
                               @Query("ArmBandId") String username,
                               @Query("Type") String type);


}