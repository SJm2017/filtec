package com.appesperta.filtec.Retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

public class RetroClient {

    private static final String ROOT_URL = "http://108.179.204.42:55/KaeService.svc/";
    private static final String QR_URL = "https://www.xpressleadpro.com/Leads/LeadsAPI/";

    private static Retrofit getRetrofitInstance(){
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(600, TimeUnit.SECONDS)
                .connectTimeout(600, TimeUnit.SECONDS)
                .build();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(loggingInterceptor);
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okHttpClient)
                .addConverterFactory(new ToStringConverterFactory())
                .build();
    }

    private static Retrofit getQrRetrofitInstance(){
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(loggingInterceptor);
        return new Retrofit.Builder()
                .baseUrl(QR_URL)
                .client(clientBuilder.build())
                .addConverterFactory(new ToStringConverterFactory())
                .build();
    }

    public static ApiService getApiService(){
        return getRetrofitInstance().create(ApiService.class);
    }
    public static ApiService getQrApiService(){
        return getQrRetrofitInstance().create(ApiService.class);
    }
}
