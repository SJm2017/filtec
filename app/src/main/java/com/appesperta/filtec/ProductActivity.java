package com.appesperta.filtec;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appesperta.filtec.Model.ProductMaster;
import com.appesperta.filtec.Product.ProductPresenterImpl;
import com.appesperta.filtec.Product.ProductView;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.List;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductActivity extends AppCompatActivity implements ProductView{
    private static final String TAG = "ProductActivity";
    @BindView(R.id.list_product)
    GridView list_product;
    @BindView(R.id.lin_single)
    LinearLayout lin_single;
    @BindView(R.id.img_product)
    ImageView img_product;
    @BindView(R.id.txt_name)
    TextView txt_name;
    @BindView(R.id.txt_desc)
    TextView txt_desc;

    ProductPresenterImpl productPresenter;
    ProgressDialog pd;
    private List<ProductMaster> productMastersList;

    String ProductId = "";
    String QuestionId = "";
    String badge = "";
    String badgedata = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);
        productPresenter = new ProductPresenterImpl(this,this);
        productPresenter.getProduct();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 2) {
                badgedata = data.getExtras().getString("data");

                StringTokenizer st = new StringTokenizer(badgedata, "^");
                if (st.countTokens() > 10) {
                    badge = st.nextToken();
                    productPresenter.GetArmBandDetails(badge);
                } else {
                    badge = "";
                }
            }
        }
    }

    @Override
    public void SetView(List<ProductMaster> productMastersList) {
        this.productMastersList = productMastersList;
        if (productMastersList.size()>1) {
            lin_single.setVisibility(View.GONE);
            list_product.setVisibility(View.VISIBLE);
            ProductListAdapter productListAdapter = new ProductListAdapter();
            list_product.setAdapter(productListAdapter);
            list_product.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    ProductId = ProductActivity.this.productMastersList.get(i).getProductId();
                    QuestionId = ProductActivity.this.productMastersList.get(i).getQuestionId();
                    Intent intent = new Intent(getApplicationContext(), Barcode_Que_Activity.class);
                    startActivityForResult(intent, 2);
                }
            });
        } else {
            list_product.setVisibility(View.GONE);
            lin_single.setVisibility(View.VISIBLE);
            lin_single.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProductId = ProductActivity.this.productMastersList.get(0).getProductId();
                    QuestionId = ProductActivity.this.productMastersList.get(0).getQuestionId();
                    Intent intent = new Intent(getApplicationContext(), Barcode_Que_Activity.class);
                    startActivityForResult(intent, 2);
                }
            });

            File file = new File(getCacheDir() + "/image/" + Uri.parse(productMastersList.get(0).getProductImage()).getLastPathSegment());
            if (file.exists()) {
                Glide.with(ProductActivity.this)
                        .load(file)
                        .placeholder(R.drawable.ic_launcher_foreground)
                        .error(R.drawable.ic_launcher_foreground)
                        .into(img_product);
            } else {
                Glide.with(ProductActivity.this)
                        .load(productMastersList.get(0).getProductImage())
                        .placeholder(R.drawable.ic_launcher_foreground)
                        .error(R.drawable.ic_launcher_foreground)
                        .into(img_product);
            }
            txt_name.setText(productMastersList.get(0).getProductName());
            txt_desc.setText(productMastersList.get(0).getDescription());
        }
    }

    int i=0;
    @OnClick(R.id.img_logo)
    void onClickLogo(){
        i++;
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                i = 0;
            }
        };
        if (i == 1) {
            handler.postDelayed(r, 500);
        } else if (i == 3) {
            i = 0;
            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {}

    @Override
    public void showProgressDialog() {
        pd = new ProgressDialog(ProductActivity.this);
        pd.setMessage(getString(R.string.please_wait));
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    @Override
    public void hideProgressDialog() {
        try {
            if (pd != null && pd.isShowing()) {
                pd.dismiss();
            }
        } catch (final IllegalArgumentException e) {
            // Handle or log or ignore
            e.printStackTrace();
        } catch (final Exception e) {
            // Handle or log or ignore
        } finally {
            pd = null;
        }
    }

    @Override
    public void showSnackBar(String resId) {
        Toast.makeText(getApplicationContext(), resId, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToHome() {
        Intent intent = new Intent(getApplicationContext(),QueAnsActivity.class);
        Log.e(TAG, "navigateToHome: ProductId : "+ProductId);
        intent.putExtra("ProductId", ProductId);
        intent.putExtra("QuestionId", QuestionId);
        startActivity(intent);
    }

    @Override
    public void navigateToRegistration() {
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        intent.putExtra("data", badgedata);
        intent.putExtra("ProductId", ProductId);
        intent.putExtra("QuestionId", QuestionId);
        startActivity(intent);
    }

    private class ProductListAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        private ProductListAdapter() {
            super();
            mInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            return productMastersList.size();
        }

        @Override
        public ProductMaster getItem(int i) {
            return productMastersList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder v;
            if (view == null) {
                view = mInflater.inflate(R.layout.row_product, null);
                v = new ViewHolder();
                v.img_product = view.findViewById(R.id.img_product);
                v.txt_name = view.findViewById(R.id.txt_name);
                v.txt_desc = view.findViewById(R.id.txt_desc);
                view.setTag(v);
            } else {
                v = (ViewHolder) view.getTag();
            }

            File file = new File(getCacheDir() + "/image/" + Uri.parse(productMastersList.get(i).getProductImage()).getLastPathSegment());
            if (file.exists()) {
                Glide.with(ProductActivity.this)
                        .load(file)
                        .placeholder(R.drawable.ic_launcher_foreground)
                        .error(R.drawable.ic_launcher_foreground)
                        .into(v.img_product);
            } else {
                Glide.with(ProductActivity.this)
                        .load(productMastersList.get(i).getProductImage())
                        .placeholder(R.drawable.ic_launcher_foreground)
                        .error(R.drawable.ic_launcher_foreground)
                        .into(v.img_product);
            }
            v.txt_name.setText(productMastersList.get(i).getProductName());
            v.txt_desc.setText(productMastersList.get(i).getDescription());

            return view;
        }

        class ViewHolder {
            ImageView img_product;
            TextView txt_name;
            TextView txt_desc;
        }
    }
}
