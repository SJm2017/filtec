package com.appesperta.filtec.Service;

import android.content.Context;
import android.util.Log;

import com.appesperta.filtec.Application;
import com.appesperta.filtec.Database.LocalDatabase;
import com.appesperta.filtec.Model.LogMaster;
import com.appesperta.filtec.Retrofit.ApiService;
import com.appesperta.filtec.Retrofit.RetroClient;
import com.appesperta.filtec.utils.InternetConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaveSyncThread implements Runnable {

    private static final String TAG = "SaveSyncThread";
    private LocalDatabase DB;
    private Context context;


    public SaveSyncThread(Context context) {
        this.context = context;
        this.DB = new LocalDatabase(context.getApplicationContext());
    }

    @Override
    public void run() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();

        Application.Sync_s= true;

        if (InternetConnection.checkConnection(context)) {
            String query = "select QuestionId,AnswerId,CompanyId,ArmBandId,EventId,SetId,ProductId from " + LogMaster.KEY_TABLE_NAME + " where IsSync = '0'";
            List<LogMaster> logMasterList = DB.getSummaryList(query);

            if (logMasterList.size() > 0) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    for (LogMaster logMaster : logMasterList) {
                        JSONObject j = new JSONObject();
                        j.put(LogMaster.KEY_QUESTION_ID, logMaster.getQuestionId());
                        j.put(LogMaster.KEY_ANSWER_ID, logMaster.getAnswerId());
                        j.put(LogMaster.KEY_COMPANY_ID, logMaster.getCompanyId());
                        j.put(LogMaster.KEY_ARMBAND_ID, logMaster.getArmBandId());
                        j.put(LogMaster.KEY_EVENT_ID, logMaster.getEventId());
                        j.put(LogMaster.KEY_SET_ID, logMaster.getSetId());
                        j.put(LogMaster.KEY_PRODUCT_ID, logMaster.getProductId());
                        jsonArray.put(j);
                    }

                    jsonObject.put("lstMultipleAnswers", jsonArray);

                    Log.e(TAG, "run: jsonObject " + jsonObject.toString());
                    RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

                    ApiService api = RetroClient.getApiService();
                    Call<String> call = api.updateSummary(body);

                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Log.e(TAG, "onResponse: " + response.body());
                            DB.updateSummaryDatabase();
                            Application.getInstant().getThread_s().start();
                            Application.isUpdate = true;
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Log.e(TAG, "onFailure: " + t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Log.e(TAG, "run: no data to update from " + LogMaster.KEY_TABLE_NAME);
            }
        }
        Application.getInstant().getHandler().postDelayed(this,20000);
    }
}
