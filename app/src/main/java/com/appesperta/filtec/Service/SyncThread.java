package com.appesperta.filtec.Service;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;


import com.appesperta.filtec.Application;
import com.appesperta.filtec.Database.LocalDatabase;
import com.appesperta.filtec.Model.LogMaster;
import com.appesperta.filtec.Retrofit.ApiService;
import com.appesperta.filtec.Retrofit.RetroClient;
import com.appesperta.filtec.utils.InternetConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncThread implements Runnable {

    private static final String TAG = "SyncThread";
    private LocalDatabase DB;
    private Context context;

    public SyncThread(Context context) {
        this.context = context;
        this.DB = new LocalDatabase(context.getApplicationContext());
    }

    @Override
    public void run() {
        if (InternetConnection.checkConnection(context)) {
            ApiService api = RetroClient.getApiService();

            String query = "SELECT Max(ModifiedDate) as date FROM QuestionAnswersLogMaster";
            String date = DB.getDate(query);
            date = TextUtils.isEmpty(date) ? "" : date;
            Log.e(TAG, "run: date " + date);
            Call<String> call = api.getAllAnswerByQuestionId(Application.CompanyId, Application.EventId, date);

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.e(TAG, "onResponse: " + response.isSuccessful());
                    if (response.isSuccessful()) {
                        getJsonData(response.body());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                }
            });
        }
    }

    private void getJsonData(String body) {
        Log.e(TAG, "getJsonData: "+ body );
        Log.e(TAG, "getJsonData: "+ body );
        try {
            JSONObject jsonObject = new JSONObject(body);
            boolean flag = jsonObject.optBoolean(LogMaster.KEY_FLAG);
            if (flag) {
                JSONArray jsonArray = jsonObject.getJSONArray(LogMaster.KEY_JSON_ARRAY);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject j = jsonArray.getJSONObject(i);

                    LogMaster logMaster = new LogMaster();
                    logMaster.setQuestionId(j.optString(LogMaster.KEY_QUESTION_ID));
                    logMaster.setAsmValue(j.optString(LogMaster.KEY_ASM_VALUE));
                    logMaster.setAnswer(j.optString(LogMaster.KEY_ANSWER));
                    logMaster.setAnswerId(j.optString(LogMaster.KEY_ANSWER_ID));
                    logMaster.setAnswerImage(j.optString(LogMaster.KEY_ANSWER_IMAGE));
                    logMaster.setCompanyId(j.optString(LogMaster.KEY_COMPANY_ID));
                    logMaster.setEventId(j.optString(LogMaster.KEY_EVENT_ID));
                    logMaster.setPieAnswerDesc(j.optString(LogMaster.KEY_PIE_ANSWER_DESC));
                    logMaster.setResultDesc(j.optString(LogMaster.KEY_RESULT_DESC));
                    logMaster.setResultTitle(j.optString(LogMaster.KEY_RESULT_TITLE));
                    logMaster.setModifiedDate(j.optString(LogMaster.KEY_MODIFIED_DATE));
                    logMaster.setUserId("");
                    logMaster.setArmBandId(j.optString(LogMaster.KEY_ARMBAND_ID));
                    logMaster.setSetId(j.optString(LogMaster.KEY_SET_ID));
                    logMaster.setProductId(j.optString(LogMaster.KEY_PRODUCT_ID));
                    DB.addLogMaster(logMaster,true);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
