package com.appesperta.filtec.RegModel;

import org.json.JSONObject;

public interface RegView {

    void showProgressDialog();
    void hideProgressDialog();
    void showSnackBar(String resId);
    void navigateToHome();
    void navigateToRegistration();
    void navigateToQuestion(String userid);

}
