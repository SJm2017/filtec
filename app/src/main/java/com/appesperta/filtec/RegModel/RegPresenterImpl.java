package com.appesperta.filtec.RegModel;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.appesperta.filtec.Application;
import com.appesperta.filtec.Database.LocalDatabase;

import com.appesperta.filtec.Main.MainView;
import com.appesperta.filtec.MainActivity;
import com.appesperta.filtec.Model.ArmbandMaster;
import com.appesperta.filtec.Model.ProductMaster;
import com.appesperta.filtec.Model.QuestionMaster;
import com.appesperta.filtec.Model.SetList;
import com.appesperta.filtec.Retrofit.ApiService;
import com.appesperta.filtec.Retrofit.RetroClient;
import com.appesperta.filtec.utils.InternetConnection;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.StringTokenizer;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegPresenterImpl implements RegPresenter {

    private static final String TAG = "RegPresenterImpl";
    private Context context;
    private RegView mainView;
    private LocalDatabase DB;

    public RegPresenterImpl(Context context, RegView mainView) {
        this.context = context;
        this.mainView = mainView;
        DB = new LocalDatabase(context);
    }


    @Override
    public void saveRegistraion(ArmbandMaster armband, String productId, String Comment) {
        if (InternetConnection.checkConnection(context.getApplicationContext())) {
            mainView.showProgressDialog();
            ApiService api = RetroClient.getApiService();

            Log.e("getUserName", "getUserName : " + armband.getUserName());
            StringTokenizer stringTokenizer = new StringTokenizer(armband.getUserName(), " ");
            String fName, lName, Email = "";
            if (stringTokenizer.countTokens() >= 2) {
                fName = stringTokenizer.nextToken();
                lName = stringTokenizer.nextToken();
            } else {
                fName = stringTokenizer.nextToken();
                lName = "";
            }


            Call<String> call = api.doRegistration(Application.CompanyId, Application.EventId,
                    armband.getArmBandID(), fName, lName, armband.getMobileNo(),
                    armband.getEmailId(), armband.getCompanyName(), armband.getBadge(), "Flitec");

            final ArmbandMaster armbandMaster = armband;
            Log.e(TAG, "doRegistration: " + call.request().url().toString());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.e(TAG, "onResponse: " + response.message());
                    Log.e(TAG, "onResponse: " + response.errorBody());
                    parseJson(response.body(), armbandMaster);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    mainView.hideProgressDialog();
                    Log.e(TAG, "onFailure: " + t.getMessage());
                }
            });
        }
    }


    private void parseJson(String response, final ArmbandMaster armbandMaster) {
        if (!TextUtils.isEmpty(response)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                boolean code = jsonObject.optBoolean("Flag");
                Log.e(TAG, "parseJson: response " + jsonObject.toString());

                if (code) {
                    armbandMaster.setSync(true);
                    armbandMaster.setUserID(jsonObject.optString("UserId"));
                    DB.addArmbandMaster(armbandMaster);

                    if (!armbandMaster.getComment().isEmpty()) {
                        try {
                            JSONObject j = new JSONObject();
                            JSONObject j1 = new JSONObject();
                            j1.put("UserId", armbandMaster.getUserID());
                            j1.put("CompanyId", Application.CompanyId);
                            j1.put("EventId", Application.EventId);
                            j1.put("Comment", armbandMaster.getComment());
                            j.put("objcomments", j1);

                            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), j.toString());
                            ApiService api = RetroClient.getApiService();
                            Call<String> call = api.SaveComments(body);
                            call.enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    Log.e(TAG, "onResponse: " + response.body());
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    Log.e(TAG, "onFailure: " + t.getMessage());
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if (!armbandMaster.getOtherData().isEmpty()) {
                        try {
                            // JSONObject badgeData = new JSONObject(armbandMaster.getOtherData().replace("BCARDData", ""));
                            JSONObject jsonleadinfo = new JSONObject();
                            jsonleadinfo.put("UserId", armbandMaster.getUserID());
                            jsonleadinfo.put("ArmBandId", armbandMaster.getArmBandID());
                            jsonleadinfo.put("EventId", Application.EventId);
                            jsonleadinfo.put("CompanyId", Application.CompanyId);

                            StringTokenizer stringTokenizer = new StringTokenizer(armbandMaster.getUserName(), " ");
                            String fName, lName;
                            if (stringTokenizer.countTokens() >= 2) {
                                fName = stringTokenizer.nextToken();
                                lName = stringTokenizer.nextToken();
                            } else {
                                fName = stringTokenizer.nextToken();
                                lName = "";
                            }

                            String data = armbandMaster.getOtherData().replace("^^", "^  ^");
                            data = data.replace("^^", "^  ^");
                            data = data.replace("^^", "^  ^");
                            StringTokenizer st = new StringTokenizer(data, "^");
                            //String str[] = data.split("^");
                            String sBadgeID = st.nextToken();
                            String sCustomerID = st.nextToken();
                            String sFirstName = st.nextToken();
                            String sLastName = st.nextToken();
                            String sTitle = st.nextToken();
                            String sCompany = st.nextToken();
                            String sAddress1 = st.nextToken();
                            String sAddress2 = st.nextToken();
                            String sAddress3 = st.nextToken();
                            String sCity = st.nextToken();
                            String sState = st.nextToken();
                            String sZip = st.nextToken();
                            String sCountry = st.nextToken();
                            String sPhone = st.nextToken();
                            String sMobile = st.nextToken();
                            String sEmail = st.nextToken();
                            String sRegType = st.nextToken();
                            String sVoucher = "";
                            try {
                                sVoucher = st.nextToken();
                            } catch (Exception e) {
                            }


                            jsonleadinfo.put("FirstName", fName);
                            jsonleadinfo.put("LastName", lName);
                            jsonleadinfo.put("Title", sTitle);
                            jsonleadinfo.put("Company", armbandMaster.getCompanyName());

                           /* jsonleadinfo.put("Question", context.getString(R.string.do_you_develop_games_for_mobile));
                            jsonleadinfo.put("Answer", armbandMaster.getQue1());
                            jsonleadinfo.put("HotLead", armbandMaster.getQue2());*/

                            jsonleadinfo.put("Address1", sAddress1);
                            jsonleadinfo.put("Address2", sAddress2);
                            jsonleadinfo.put("Address3", sAddress3);

                            jsonleadinfo.put("Email", armbandMaster.getEmailId());
                            jsonleadinfo.put("City", sCity);
                            jsonleadinfo.put("State", sState);
                            jsonleadinfo.put("Country", sCountry);
                            jsonleadinfo.put("ZipCode", sZip);
                            jsonleadinfo.put("Phone", armbandMaster.getPhoneNo());
                            jsonleadinfo.put("PhoneExtension", "");
                            jsonleadinfo.put("opt_promotional_events", "false");
                            jsonleadinfo.put("opt_survey", armbandMaster.getOptSurvey());

                            String str = "RegType" + "^^" + sRegType + "~~" +
                                    "Voucher" + "^^" + sVoucher + "~~" +
                                    "CustomerID" + "^^" + sCustomerID + "~~" +
                                    "sPhone" + "^^" + sPhone + "~~" +
                                    "sMobile" + "^^" + sMobile + "~~" +
                                    "sEmail" + "^^" + sEmail + "~~" +
                                    "When do you need a system in place by?" + "^^" + armbandMaster.getQue1() + "~~" +
                                    "Which inspection system are you interested in:" + "^^" + armbandMaster.getQue2() + "~~" +
                                    "Would you like one of our industry experts to contact you for a consultation?" + "^^" + armbandMaster.getOptPromotional() + "";

                            jsonleadinfo.put("OtherInformation", str);

                            JSONObject jsonOutPutObj = new JSONObject();
                            jsonOutPutObj.put("obj", jsonleadinfo);
                            Log.e(TAG, "run: jsonObject " + jsonOutPutObj.toString());
                            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonOutPutObj.toString());
                            ApiService api = RetroClient.getApiService();
                            Call<String> call = api.SaveUserInfo(body);
                            call.enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    Log.e(TAG, "onResponse: " + response.body());
                                    // DB.updateSummaryDatabase();
                                    mainView.hideProgressDialog();
                                    Application.preferences.setNfcId(armbandMaster.getArmBandID());
                                    Application.preferences.setUserId(armbandMaster.getUserID());
                                    mainView.navigateToQuestion(armbandMaster.getUserID());
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    Log.e(TAG, "onFailure: " + t.getMessage());
                                    mainView.hideProgressDialog();

                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "parseJson: response Null");
        }
    }
}
