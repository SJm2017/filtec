package com.appesperta.filtec.Main;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.appesperta.filtec.Application;
import com.appesperta.filtec.Database.LocalDatabase;
import com.appesperta.filtec.MainActivity;
import com.appesperta.filtec.Model.ProductMaster;
import com.appesperta.filtec.Model.QuestionMaster;
import com.appesperta.filtec.Model.SetList;
import com.appesperta.filtec.Retrofit.ApiService;
import com.appesperta.filtec.Retrofit.RetroClient;
import com.appesperta.filtec.utils.InternetConnection;


import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONObject;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenterImpl implements MainPresenter {

    private static final String TAG = "QrviewPresenterImpl";
    private Context context;
    private MainView mainView;
    private LocalDatabase DB;

    public MainPresenterImpl(Context context, MainView mainView) {
        this.context = context;
        this.mainView = mainView;
        DB = new LocalDatabase(context);
    }

    @Override
    public void getSet() {
        if (InternetConnection.checkConnection(context)) {
            mainView.showProgressDialog();
            ApiService api = RetroClient.getApiService();
            Call<String> call = api.getSets(Application.CompanyId, Application.EventId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    parseSetsResponse(response.body());
                    getProduct();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    mainView.hideProgressDialog();
                    Log.e(TAG, "onFailure getSet: " + t.getMessage());
                }
            });
        } else {
            MainActivity.setID.clear();
            MainActivity.setName.clear();
            DB.GetSetList();
            mainView.selectSets();
        }
    }

    private void parseSetsResponse(String response) {

        try {
            JSONObject jsonObject = new JSONObject(response);
            String code = jsonObject.optString("Flag");
            if (code.equals("success")) {
                JSONArray result = jsonObject.getJSONArray("SetList");
                for (int i = 0; i < result.length(); i++) {
                    JSONObject j = result.getJSONObject(i);
                    SetList setList = new SetList(j);
                    DB.addSetList(setList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getProduct() {
        if (InternetConnection.checkConnection(context)) {
            ApiService api = RetroClient.getApiService();
            Call<String> call = api.getProductList();
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    parseProductsResponse(response.body());
                    getAllQuestions();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e(TAG, "onFailure getProduct: " + t.getMessage());
                }
            });
        }
    }

    private void parseProductsResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            boolean code = jsonObject.optBoolean("Flag");
            if (code) {
                JSONArray result = jsonObject.getJSONArray("ProductList");
                for (int i = 0; i < result.length(); i++) {
                    JSONObject j = result.getJSONObject(i);
                    ProductMaster productMaster = new ProductMaster(j);
                    DB.addProductMaster(productMaster);
                }
            }
            new BannerDownload().execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAllQuestions() {
        if (InternetConnection.checkConnection(context)) {
            ApiService api = RetroClient.getApiService();
            Call<String> call = api.getAllQuestion(Application.CompanyId, Application.EventId, "", "");
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.e(TAG, "onResponse onSyncData: " + response.body());
                    parseSyncResponse(response.body());
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e(TAG, "onFailure onSyncData: " + t.getMessage());
                }
            });
        }
    }

    private void parseSyncResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String code = jsonObject.optString("Flag");
            if (code.equals("success")) {
                JSONArray result = jsonObject.getJSONArray("AllQuestionList");
                for (int i = 0; i < result.length(); i++) {
                    JSONObject j = result.getJSONObject(i);
                    QuestionMaster questionMaster = new QuestionMaster(j, DB);
                    DB.addQuestionMaster(questionMaster);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class BannerDownload extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(final String... params) {
            List<ProductMaster> list = DB.GetProductMasterList();
            for (int i = 0; i < list.size(); i++) {
                downloadImage(list.get(i).getProductImage(), Uri.parse(list.get(i).getProductImage()).getLastPathSegment());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            mainView.hideProgressDialog();
            MainActivity.setID.clear();
            MainActivity.setName.clear();
            DB.GetSetList();
            mainView.selectSets();

        }
    }

    private String downloadImage(String appIconUrl, String name) {
        File f = new File(context.getCacheDir() + "/image/");
        if (!f.exists()) {
            f.mkdir();
        }
        try {
            URL url = new URL(appIconUrl);
            File file = new File(f.getAbsolutePath() + "/" + name);
            Log.e(TAG, "downloadImage: " + file.getAbsolutePath());
            if (!file.exists()) {
                System.out.println("--Application Master appIconUrl : " + appIconUrl);
                URLConnection ucon = url.openConnection();
                InputStream is = ucon.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                ByteArrayBuffer baf = new ByteArrayBuffer(50);
                int current;
                while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
                }

                FileOutputStream fos = new FileOutputStream(file);
                fos.write(baf.toByteArray());
                fos.close();
                System.out.println("--Application Master appIconUrl Success " + file);
            }
            return file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
