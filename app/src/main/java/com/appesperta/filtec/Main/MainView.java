package com.appesperta.filtec.Main;

public interface MainView {

    void showProgressDialog();
    void hideProgressDialog();
    void showSnackBar(String resId);
    void navigateToHome();
    void navigateToRegistration();
    void selectSets();
}
